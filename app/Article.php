<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	protected $fillable = [
		'title', 'description', 'image','category','tags','author','slider_status,','status','meta_title','meta_description'
	];
	
	public function user()
	{
		return $this->belongsTo(User::class,'id','author');
	}
	
	public function category()
	{
		return $this->belongsTo(Category::class,'category_id','category');
	}
}
