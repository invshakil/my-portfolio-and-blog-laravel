<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Closure;

class AdminAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		$type = auth()->user()->user_type;
		
		if ($type == 1) {
			
			return $next($request);
			
		}
	
		$notification = array(
			'message' => 'Please login to your admin space!!',
			'alert-type' => 'error'
		);
		
		return Redirect::to('/user/dashboard')->with($notification);
    }
}
