<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use \File;

class Admin extends Controller
{
    public function index()
    {
        $data = array();
        $data['page_name'] = 'Admin Dashboard';
        $data['account_type'] = 'admin';

        $notification = array(
            'message' => 'Successfully Logged In!',
            'alert-type' => 'success'
        );

        $page = view('backend.admin.dashboard', $data)->with($notification);

        return view('backend.master', $data)->with('page', $page);
    }

    /*
     * PROFILE FUNCTION
     */

    public function profile(Request $request, $param = NULL, $param2 = NULL)
    {
        
        $data = array();
        $data['page_name'] = 'Admin Profile';

        //Image Upload//

        if ($param == 'image') {
            $id = $request->input('id');
            $image = DB::table('users')->where('id', $id)->value('image_path');

            //FIRST WILL CHECK IF USER HAS ANY UPLOADED IMAGE
            if ($image == NULL) {
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:512',
                ]);

                $imageName = time() . '.' . $request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads/profile_images/'), $imageName);

                $insert = array('id' => $id,
                    'image_path' => 'public/uploads/profile_images/' . $imageName,
                );

                DB::table('users')->insert($insert);

                $notification = array(
                    'message' => 'Image Uploaded Successfully!',
                    'alert-type' => 'success'
                );
                return back()->with($notification);

            } else {
                $this->validate($request, [
                    'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:512',
                ]);

                $imageName = time() . '.' . $request->image->getClientOriginalExtension();
                $request->image->move(public_path('uploads/profile_images/'), $imageName);

                $update = array('image_path' => 'public/uploads/profile_images/' . $imageName);

                DB::table('users')
                    ->where('id', $id)
                    ->update($update);

                $notification = array(
                    'message' => 'Image Updated Successfully!',
                    'alert-type' => 'success'
                );
                return back()->with($notification);
            }
        } elseif ($param == 'do_update') {
            $id = $request->input('id');
            $info = DB::table('users')->where('id', $id)->value('id');


            if ($info == NULL) {
                $this->validate($request, [
                    'profession' => 'required',
                    'address' => 'required',
                    'phone' => 'required'
                ]);

                $insert = array('id' => $id,
                    'profession' => $request->input('profession'),
                    'address' => $request->input('address'),
                    'phone' => $request->input('phone'),
                    'created_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString(),
                    'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
                );

                DB::table('users')->insert($insert);

                $notification = array(
                    'message' => 'Profile Information added successfully!',
                    'alert-type' => 'success'
                );
                return back()->with($notification);

            } else {

                $this->validate($request, [
                    'profession' => 'required',
                    'address' => 'required',
                    'phone' => 'required'
                ]);

                $data = array(
                    'profession' => $request->input('profession'),
                    'address' => $request->input('address'),
                    'phone' => $request->input('phone'),
                    'created_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString(),
                    'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
                );

                DB::table('users')->where('id', $id)->update($data);

                $notification = array(
                    'message' => 'Profile Information Updated Successfully!',
                    'alert-type' => 'success'
                );
                return back()->with($notification);
            }
        } elseif ($param == 'password') {
            $this->validate($request, [
                'password' => 'required|min:6',
                'confirm_password' => 'required|same:password'
            ]);

            $data = array(
                'password' => $request->input('password')
            );
            $id = $request->input('id');
            DB::table('users')->where('id', $id)->update($data);

            $notification = array(
                'message' => 'Password Updated Successfully!',
                'alert-type' => 'success'
            );
            return back()->with($notification);
        }


        $data['account_type'] = 'admin';
        $page = view('backend.admin.profile', $data);

        return view('backend.master', $data)->with('page', $page);
    }

    /*
     * BOOK CATEGORY FUNCTION
     */

    public function categories(Request $request, $param = NULL, $param2 = NULL)
    {
       
        if ($param == 'create') {
            $this->validate($request, [
                'category_name' => 'required',
                'category_description' => 'required',
                'status' => 'required'
            ]);

            $data = array(
                'category_name' => $request->input('category_name'),
                'category_description' => $request->input('category_description'),
                'status' => $request->input('status'),
                'created_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
            );

            DB::table('categories')->insert($data);

            $notification = array(
                'message' => 'Category Added Successfully!',
                'alert-type' => 'success'
            );
            return back()->with($notification);
        } elseif ($param == 'do_update') {
            $this->validate($request, [
                'category_name' => 'required',
                'category_description' => 'required',
                'status' => 'required'
            ]);

            $data = array(
                'category_name' => $request->input('category_name'),
                'category_description' => $request->input('category_description'),
                'status' => $request->input('status'),
                'created_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
            );

            $id = $request->input('category_id');

            DB::table('categories')->where('category_id', $id)->update($data);

            $notification = array(
                'message' => 'Category Updated Successfully!',
                'alert-type' => 'success'
            );

            return back()->with($notification);
        } elseif ($param == 'delete') {
            DB::table('categories')->where('category_id', $param2)->delete();
            Session::put('exception', 'Category Deleted Successfully!');

            $notification = array(
                'message' => 'Category Deleted Successfully!',
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }

        $data = array();
        $data['page_name'] = 'Book Categories';
        $data['account_type'] = 'admin';
        $page = view('backend.admin.categories', $data);

        return view('backend.master', $data)->with('page', $page);

    }

    /*
     * BOOKS UPLOAD AND MANAGE
     */

    public function articles(Request $request, $param = NULL, $param2 = NULL)
    {
        
        if ($param == 'create') {
            $this->validate($request, [
                'title' => 'required',
                'description' => 'required',
                'image' => 'required',
                'category' => 'required',
                'tags' => 'required',
                'meta_title' => 'nullable',
                'meta_description' => 'nullable'
            ]);

            $category = $request->input('category');

            $categories = implode(',', $category);

            
            /*
             * FIRST CHECKING IF USER HAS UPLOADED ANY IMAGE
             * IF YES THEN IT IS GENERATING 3 VERSION OF AN IMAGE
             * AND GETTING THE IMAGE PATH
             */
            
            if ($request->file('image')){
	
				//Getting image file and setting the name of image with extension
				$photo = $request->file('image');
				$image_name = time().'.'.$photo->getClientOriginalExtension();
	
				//Setting up the image path
				$destinationPath = public_path('/uploads/article_image/');
				$photo->move($destinationPath, $image_name);
	
				$img = Image::make('public/uploads/article_image/'.$image_name);
				//Image resizing with aspect ratio and saving it to the directory
				$img->resize(null, 440,function ($constraint) {
					$constraint->aspectRatio();
				});
				$img->save($destinationPath.'/'.$image_name,85);
				$image_path = 'public/uploads/article_image/'.$image_name;
	
				//Medium Size Image conversion
	
				$m_path = public_path('/uploads/article_image/m_size/');
	
				//Image resizing with aspect ratio and saving it to the directory
				$img->resize(null, 160,function ($constraint) {
					$constraint->aspectRatio();
				});
				$img->save($m_path.'/'.$image_name,70);
				$m_path = 'public/uploads/article_image/m_size/'.$image_name;
	
				// Thumbnail Size conversion
	
				$thumbs_path = public_path('/uploads/article_image/re_sized/');
	
				//Image resizing with aspect ratio and saving it to the directory
				$img->resize(null, 70,function ($constraint) {
					$constraint->aspectRatio();
				});
				$img->save($thumbs_path.'/'.$image_name,70);
				$thumb_path = 'public/uploads/article_image/re_sized/'.$image_name;
    
			}
			
			/*
			 * HERE WE ARE CHECKING IF USER HAS ONLY
			 */
			
			else {
				
				$image_path = $request->input('image');
				$image = explode('/',$image_path);
				$image_name = $image[3];
				
				$image_path = 'public/uploads/article_image/'.$image_name;
				$m_path = 'public/uploads/article_image/m_size/'.$image_name;
				$thumb_path = 'public/uploads/article_image/re_sized/'.$image_name;
			}

            

            $data = array(
                'title' => $request->input('title'),
                'description' => $request->input('description'),
                'author' => auth()->user()->id,
                'category' => $categories,
                'tags' => $request->input('tags'),
                'meta_title' => $request->input('meta_title'),
                'meta_description' => $request->input('meta_description'),
                'status' => $request->input('status'),
                'slider_status' => $request->input('slider_status'),
                'image' => $image_path,
                'thumbnail' => $thumb_path,
                'm_size_img' => $m_path,
                'created_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
            );

            DB::table('articles')->insert($data);

            $notification = array(
                'message' => 'Article saved Successfully!',
                'alert-type' => 'success'
            );
            return back()->with($notification);
        }
        
        elseif ($param == 'do_update') {
            $this->validate($request, [
                'title' => 'required',
                'description' => 'required',
                'image' => 'nullable',
                'category' => 'required',
                'tags' => 'required',
                'meta_title' => 'nullable',
                'meta_description' => 'nullable'
            ]);

            $category = $request->input('category');

            $categories = implode(',', $category);

            $description = (htmlspecialchars($request->input('description')));

            if ($request->image != NULL) {
                $id = $request->input('article_id');

                $info = DB::table('articles')->where('article_id',$id)->first();

                File::delete($info->image);
                File::delete($info->thumbnail);

                //Getting image file and setting the name of image with extension
                $photo = $request->file('image');
                $image_name = time().'.'.$photo->getClientOriginalExtension();

                //Setting up the image path
                $destinationPath = public_path('/uploads/article_image/');
                $photo->move($destinationPath, $image_name);

                $img = Image::make('public/uploads/article_image/'.$image_name);
                //Image resizing with aspect ratio and saving it to the directory
                $img->resize(null, 440,function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save($destinationPath.'/'.$image_name,85);
                $image_path = 'public/uploads/article_image/'.$image_name;

                //Medium Size Image conversion

                $m_path = public_path('/uploads/article_image/m_size/');

                //Image resizing with aspect ratio and saving it to the directory
                $img->resize(null, 160,function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save($m_path.'/'.$image_name,70);
                $m_path = 'public/uploads/article_image/m_size/'.$image_name;

                // Thumbnail Size conversion

                $thumbs_path = public_path('/uploads/article_image/re_sized/');

                //Image resizing with aspect ratio and saving it to the directory
                $img->resize(null, 70,function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save($thumbs_path.'/'.$image_name,70);
                $thumb_path = 'public/uploads/article_image/re_sized/'.$image_name;



                $data = array(
                    'title' => $request->input('title'),
                    'description' => $categories,
                    'author' => auth()->user()->id,
                    'category' => $request->input('category'),
                    'tags' => $request->input('tags'),
                    'meta_title' => $request->input('meta_title'),
                    'meta_description' => $request->input('meta_description'),
                    'status' => $request->input('status'),
                    'slider_status' => $request->input('slider_status'),
                    'image' => $image_path,
                    'thumbnail' => $thumb_path,
                    'm_size_img' => $m_path,
                    'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
                );



                DB::table('articles')->where('article_id', $id)->update($data);
                $notification = array(
                    'message' => 'Article\'s Information Updated Successfully!',
                    'alert-type' => 'success'
                );
                return back()->with($notification);
            } else {

                $data = array(
                    'title' => $request->input('title'),
                    'description' => $description,
                    'author' => auth()->user()->id,
                    'category' => $categories,
                    'tags' => $request->input('tags'),
                    'meta_title' => $request->input('meta_title'),
                    'meta_description' => $request->input('meta_description'),
                    'status' => $request->input('status'),
                    'slider_status' => $request->input('slider_status'),
                    'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
                );

                $id = $request->input('article_id');
                DB::table('articles')->where('article_id', $id)->update($data);
            }
            $notification = array(
                'message' => 'Article\'s Information Updated Successfully!',
                'alert-type' => 'success'
            );
            return back()->with($notification);
        } elseif ($param == 'delete') {

            DB::table('articles')->where('article_id', $param2)->delete();

            $notification = array(
                'message' => 'Article\'s Information Deleted Successfully!',
                'alert-type' => 'success'
            );
            return back()->with($notification);
        }

        $data = array();
        $data['page_name'] = 'Articles';
        $data['account_type'] = 'admin';
        $page = view('backend.admin.articles', $data);

        return view('backend.master', $data)->with('page', $page);
    }

    public function uploaded_image()
    {
        

        $data = array();
        $data['page_name'] = 'Image Gallery';

        $data['account_type'] = 'admin';
        $page = view('backend.admin.uploaded_image', $data);

        return view('backend.master', $data)->with('page', $page);
    }

    function articleById($id)
    {


        $data = array();
        $data['id'] = $id;
        $data['page_name'] = 'Articles';
        $data['account_type'] = 'admin';
        $page = view('backend.admin.edit_pages.edit_article', $data);
        
        return view('backend.master', $data)->with('page', $page);
    }
}
