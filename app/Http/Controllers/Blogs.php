<?php

namespace App\Http\Controllers;

use App\Article;
use App\Subscribers;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Mail;


class Blogs extends Controller
{
    public function index()
    {
        $data = array();

        $data['page_title'] = 'Blogs of Shakil';

        $main_content = view('frontEnd.blogs.index_content', $data);
        return view('frontEnd.blogs.master', $data)->with('main_content', $main_content);
    }

    public function article($id)
    {
        $data = array();

        $data['details'] = DB::table('articles as a')
            ->where('article_id', $id)
            ->join('categories as c', 'a.category', '=', 'c.category_id')
            ->join('users as u', 'a.author', '=', 'u.id')
            ->select('a.*', 'u.name', 'u.id', 'c.category_name')
            ->first();

        $data['page_title'] = $data['details']->title . ' || Shakil&#39;s Blog';

        $data['image'] = $data['details']->image;
        $data['description'] = $data['details']->meta_description;
        $data['keywords'] = $data['details']->tags;


        $fb_id = DB::table('social_facebook_accounts')->where('user_id', $data['details']->id)->value('provider_user_id');
        if ($fb_id) {
            $data['author'] = 'https://www.facebook.com/' . $fb_id;
        }

        $main_content = view('frontEnd.blogs.article', $data);
        return view('frontEnd.blogs.master', $data)->with('main_content', $main_content);
    }

    public function category($id)
    {
        $data = array();

        $data['category_name'] = DB::table('categories')->where('category_id', $id)->value('category_name');

        //FIND_IN_SET will find specific id from a set of value from database field.

        $data['category'] = DB::table('articles as a')
            ->join('categories as c', 'a.category', '=', 'c.category_id')
            ->join('users as u', 'a.author', '=', 'u.id')
            ->latest()
            ->where('a.status', 1)
//            ->where('a.category', $id)
            ->whereRaw('FIND_IN_SET('.$id.',a.category)')
            ->select('a.*', 'u.name', 'c.category_name')
            ->paginate(10);

        $data['page_title'] = $data['category_name'] . ' || Shakil&#39;s Blog';

        $main_content = view('frontEnd.blogs.category', $data);
        return view('frontEnd.blogs.master', $data)->with('main_content', $main_content);
    }

    public function contact(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'contact' => 'nullable',
            'message' => 'required|min:10',
        ]);

        $data = array();
        $data['name'] = $request->input('name');
        $data['email'] = $request->input('email');
        $data['contact'] = $request->input('contact');
        $data['MailBody'] = $request->input('message');

        $data['subject'] = 'Mail From Your Blog Contact!';

        Mail::send('email.contact', $data, function ($m) use ($data) {
            $m->from('contact@sshakil.com', $data['name']);
            $m->to('inverse.shakil@gmail.com', 'www.sshakil.com')->subject($data['subject']);
        });

        $notification = array(
            'message' => 'Thanks for contacting with me. I will get back to you soon!',
            'alert-type' => 'success'
        );
        return back()->with($notification);

    }

    public function subscribe(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:subscribers',
        ],
            [
                'email.unique' => 'You are already Subscribed to this website content!'
            ]);

        $subscriber = new Subscribers;

        $subscriber->email = $request->email;
        $subscriber->status = 1;

        $subscriber->save();


        $data['email'] = $request->input('email');


        $data['subject'] = "Mail From Shakil's Blog!";

        Mail::send('email.success_subscribe', $data, function ($m) use ($data) {
            $m->from('contact@sshakil.com', 'Shakil');
            $m->to($data['email'] , 'www.sshakil.com')->subject($data['subject']);
        });

        $notification = array(
            'message' => 'Thanks for subscribing. We will update you as soon as new content published!',
            'alert-type' => 'success'
        );
        return back()->with($notification);
    }
    
    function search(Request $request)
	{
		$query = $request->input('search');
		
		$data['search'] = $query;
		
		$data['details'] = DB::table('articles as a')->where('a.title', 'LIKE', '%' . $query . '%')->orWhere('a.description', 'LIKE', '%' . $query . '%')
			->join('categories as c', 'a.category', '=', 'c.category_id')
			->join('users as u', 'a.author', '=', 'u.id')
			->select('a.*', 'u.name', 'u.id', 'c.category_name')
			->paginate(10);
		
		$data['page_title'] = 'Search result of '.$data['search'] .' || Shakil&#39;s Blog';
		
		$main_content = view('frontEnd.blogs.search_result', $data);
		return view('frontEnd.blogs.master', $data)->with('main_content', $main_content);
	}
	
	public function archives(Request $request)
	{
		
		$month = $request->month;
		$year = $request->year;
		
		$data['archive_name'] = 'Archives from '.$month.', '.$year;
		
		$articles = Article::latest();
		$articles->whereMonth('created_at', Carbon::parse($month)->month);
		$articles->whereYear('created_at', $year);
		
		
		
		$data['details'] = $articles->paginate(10);
		
		$data['page_title'] =  'Archives || Shakil&#39;s Blog';
		
		$main_content = view('frontEnd.blogs.archives', $data);
		return view('frontEnd.blogs.master', $data)->with('main_content', $main_content);
	
	}
	
	public function getArticleByTag($tag)
	{
		$tag = str_replace('-', ' ', $tag);
		
		$data['archive_name'] = 'Articles on '.$tag.' Tag';
		
		$articles = Article::latest();
		
		$articles->whereRaw("FIND_IN_SET('$tag', tags)");
		
		$data['details'] = $articles->paginate(10);
		
		$data['page_title'] =  'Tag Result of'.$tag.' || Shakil&#39;s Blog';
		
		$main_content = view('frontEnd.blogs.archives', $data);
		return view('frontEnd.blogs.master', $data)->with('main_content', $main_content);
	}
}
