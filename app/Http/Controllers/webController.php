<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class webController extends Controller
{
    public function index()
    {
        return view('frontEnd.portfolio.master');
    }

    public function projectGallery($id)
    {
        $data = array();

        $data['details'] = DB::table('projects')
            ->where('project_id', $id)
            ->first();

        $data['page_title'] = $data['details']->title.' || SHAKIL';

        $main_content = view('frontEnd.blogs.project_details', $data);

        return view('frontEnd.blogs.master', $data)->with('main_content',$main_content);
    }
}
