<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;

class UserLogin extends Controller
{
    public function index()
    {

        $type = Session::get('login_type');

        if ($type == 1){

            $notification = array(
                'message' => 'You are Admin of this system!',
                'alert-type' => 'warning'
            );
            return Redirect::to('/admin_dashboard')->with($notification);
        }
        if($type == 2){
            $notification = array(
                'message' => 'You are Member of this system!',
                'alert-type' => 'warning'
            );
            return Redirect::to('/user_dashboard')->with($notification);
        }

        $data = array();

        $data['page_title'] = 'User Login';

        return view('backend.login',$data);
    }

    public function ajax_login(Request $request)
    {
        $this->validate($request,[
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $email = $request->email;
        $password = md5($request->password);

        $query = DB::table('users')
            ->where('email', $email)
            ->where('password', $password)
            ->first();

        if ($query)
        {

            if ($query->user_type == 1)
            {
                Session::put('id',$query->id);
                Session::put('name',$query->name);
                Session::put('login_type',1);
                $notification = array(
                    'message' => 'Successfully Logged In!',
                    'alert-type' => 'success'
                );
                return Redirect::to('admin_dashboard')->with($notification);
            }

            else
            {
                Session::put('id',$query->id);
                Session::put('name',$query->name);
                Session::put('login_type',4);
                return Redirect::to('user_dashboard');
            }
        }
        else
        {
            $notification = array(
                'message' => 'Invalid User Email or Password! Try Again.',
                'alert-type' => 'error'
            );
            return Redirect::to('login')->with($notification);
        }

    }


    public function logout()
    {
        Session::put('id',null);
        Session::put('name',null);
        Session::put('login_type', null);


        $notification = array(
            'message' => 'You Are Successfully Logged Out!',
            'alert-type' => 'warning'
        );

        return Redirect::to('/login')->with($notification);

    }
}
