<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class Modal extends Controller
{
    function popup($page_name = '' , $param2 = '' , $param3 = '')
    {
        $account_type = 'admin';
        $page_data['param2']		=	$param2;
        $page_data['param3']		=	$param3;

        echo '<script src=" '.url('public/assets/js/neon-custom-ajax.js'). '"></script>';

        return view('backend/'.$account_type.'/modal'.'/'.$page_name, $page_data);
    }
}
