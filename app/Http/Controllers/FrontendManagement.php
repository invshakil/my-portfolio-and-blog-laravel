<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;


class FrontendManagement extends Controller
{
    public function index(Request $request, $param = NULL)
    {


        $data = array();
        $data['page_name'] = 'Banner Content';

        if ($param == 'do_update') {

            if ($request->image != NULL) {
                $this->validate($request, [
                    'name' => 'required',
                    'designation' => 'required',
                    'intro_text' => 'required',
                    'image' => 'required'
                ]);

                $name = array( 'description' => $request->input('name') );
                DB::table('banner_content')->where('info_type','name')->update($name);

                $des = array( 'description' => $request->input('designation') );
                DB::table('banner_content')->where('info_type','designation')->update($des);

                $intro = array( 'description' => $request->input('intro_text') );
                DB::table('banner_content')->where('info_type','intro_text')->update($intro);

                $facebook = array( 'description' => $request->input('facebook') );
                DB::table('banner_content')->where('info_type','facebook')->update($facebook);

                $twitter = array( 'description' => $request->input('twitter') );
                DB::table('banner_content')->where('info_type','twitter')->update($twitter);

                $bitbucket = array( 'description' => $request->input('bitbucket') );
                DB::table('banner_content')->where('info_type','bitbucket')->update($bitbucket);

                $stack_overflow = array( 'description' => $request->input('stack_overflow') );
                DB::table('banner_content')->where('info_type','stack_overflow')->update($stack_overflow);

                //Getting image file and setting the name of image with extension
                $photo = $request->file('image');
                $image_name = time().'.'.$photo->getClientOriginalExtension();

                //Setting up the image path
                $destinationPath = public_path('/uploads/profile_images/');
                $photo->move($destinationPath, $image_name);

                $img = Image::make('public/uploads/profile_images/'.$image_name);
                //Image resizing with aspect ratio and saving it to the directory
                $img->resize(500, null,function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save($destinationPath.'/'.$image_name,80);

                $image_path = 'public/uploads/profile_images/'.$image_name;
                $image = array( 'description' => $image_path );
                DB::table('banner_content')->where('info_type','image')->update($image);

                $notification = array(
                    'message' => 'Banner Content Saved successfully!',
                    'alert-type' => 'success'
                );
                return back()->with($notification);

            } else {

                $this->validate($request, [
                    'name' => 'required',
                    'designation' => 'required',
                    'intro_text' => 'required',
                ]);

                $name = array( 'description' => $request->input('name') );
                DB::table('banner_content')->where('info_type','name')->update($name);

                $des = array( 'description' => $request->input('designation') );
                DB::table('banner_content')->where('info_type','designation')->update($des);

                $intro = array( 'description' => $request->input('intro_text') );
                DB::table('banner_content')->where('info_type','intro_text')->update($intro);

                $facebook = array( 'description' => $request->input('facebook') );
                DB::table('banner_content')->where('info_type','facebook')->update($facebook);

                $twitter = array( 'description' => $request->input('twitter') );
                DB::table('banner_content')->where('info_type','twitter')->update($twitter);

                $bitbucket = array( 'description' => $request->input('bitbucket') );
                DB::table('banner_content')->where('info_type','bitbucket')->update($bitbucket);

                $stack_overflow = array( 'description' => $request->input('stack_overflow') );
                DB::table('banner_content')->where('info_type','stack_overflow')->update($stack_overflow);

                $notification = array(
                    'message' => 'Banner Content Information Saved Successfully!',
                    'alert-type' => 'success'
                );
                return back()->with($notification);
            }
        }


        $data['account_type'] = 'admin';
        $page = view('backend.admin.banner', $data);

        return view('backend.master', $data)->with('page', $page);
    }


    public function about(Request $request, $param = NULL)
    {
        
        $data = array();
        $data['page_name'] = 'About Content';

        if ($param == 'do_update') {

            if ($request->image != NULL) {
                $this->validate($request, [
                    'name' => 'required',
                    'age' => 'required',
                    'experience' => 'required',
                    'image' => 'required',
                    'email' => 'required',
                    'phone' => 'required',
                    'website' => 'required',
                    'address' => 'required',
                    'cv' => 'required',
                    'blog' => 'required',
                    'video' => 'nullable',
                    'about' => 'nullable',

                ]);

                $name = array( 'description' => $request->input('name') );
                DB::table('about_content')->where('info_type','name')->update($name);

                $age = array( 'description' => $request->input('age') );
                DB::table('about_content')->where('info_type','age')->update($age);

                $experience = array( 'description' => $request->input('experience') );
                DB::table('about_content')->where('info_type','experience')->update($experience);

                $email = array( 'description' => $request->input('email') );
                DB::table('about_content')->where('info_type','email')->update($email);

                $website = array( 'description' => $request->input('website') );
                DB::table('about_content')->where('info_type','website')->update($website);

                $address = array( 'description' => $request->input('address') );
                DB::table('about_content')->where('info_type','address')->update($address);

                $phone = array( 'description' => $request->input('phone') );
                DB::table('about_content')->where('info_type','phone')->update($phone);

                $cv = array( 'description' => $request->input('cv') );
                DB::table('about_content')->where('info_type','cv')->update($cv);

                $blog = array( 'description' => $request->input('blog') );
                DB::table('about_content')->where('info_type','blog')->update($blog);

                $video = array( 'description' => $request->input('video') );
                DB::table('about_content')->where('info_type','video')->update($video);

                $about = array( 'description' => $request->input('about') );
                DB::table('about_content')->where('info_type','about')->update($about);

                //Getting image file and setting the name of image with extension
                $photo = $request->file('image');
                $image_name = time().'.'.$photo->getClientOriginalExtension();

                //Setting up the image path
                $destinationPath = public_path('/uploads/profile_images/');
                $photo->move($destinationPath, $image_name);

                $img = Image::make('public/uploads/profile_images/'.$image_name);
                //Image resizing with aspect ratio and saving it to the directory
                $img->resize(500, null,function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save($destinationPath.'/'.$image_name,80);

                $image_path = 'public/uploads/profile_images/'.$image_name;
                $image = array( 'description' => $image_path );
                DB::table('about_content')->where('info_type','image')->update($image);

                $notification = array(
                    'message' => 'About Content Saved successfully!',
                    'alert-type' => 'success'
                );
                return back()->with($notification);

            } else {

                $this->validate($request, [
                    'name' => 'required',
                    'age' => 'required',
                    'experience' => 'required',
                    'email' => 'required',
                    'phone' => 'required',
                    'website' => 'required',
                    'address' => 'required',
                    'cv' => 'required',
                    'blog' => 'required',
                    'video' => 'nullable',
                    'about' => 'nullable',
                ]);

                $name = array( 'description' => $request->input('name') );
                DB::table('about_content')->where('info_type','name')->update($name);

                $age = array( 'description' => $request->input('age') );
                DB::table('about_content')->where('info_type','age')->update($age);

                $experience = array( 'description' => $request->input('experience') );
                DB::table('about_content')->where('info_type','experience')->update($experience);

                $email = array( 'description' => $request->input('email') );
                DB::table('about_content')->where('info_type','email')->update($email);

                $website = array( 'description' => $request->input('website') );
                DB::table('about_content')->where('info_type','website')->update($website);

                $address = array( 'description' => $request->input('address') );
                DB::table('about_content')->where('info_type','address')->update($address);

                $phone = array( 'description' => $request->input('phone') );
                DB::table('about_content')->where('info_type','phone')->update($phone);

                $cv = array( 'description' => $request->input('cv') );
                DB::table('about_content')->where('info_type','cv')->update($cv);

                $blog = array( 'description' => $request->input('blog') );
                DB::table('about_content')->where('info_type','blog')->update($blog);

                $video = array( 'description' => $request->input('video') );
                DB::table('about_content')->where('info_type','video')->update($video);

                $about = array( 'description' => $request->input('about') );
                DB::table('about_content')->where('info_type','about')->update($about);

                $notification = array(
                    'message' => 'About Content Information Saved Successfully!',
                    'alert-type' => 'success'
                );
                return back()->with($notification);
            }
        }


        $data['account_type'] = 'admin';
        $page = view('backend.admin.about', $data);

        return view('backend.master', $data)->with('page', $page);
    }

    public function skill_set(Request $request, $param = NULL)
    {
    

    }

    public function education(Request $request, $param = NULL,$param2=NULL)
    {
        

        if ($param == 'create') {
            $this->validate($request, [
                'session' => 'required',
                'institute_name' => 'required',
                'degree_name' => 'required',
                'subject' => 'required',
                'result' => 'required',
                'status' => 'required',
            ]);

            $data = array(
                'session' => $request->input('session'),
                'institute_name' => $request->input('institute_name'),
                'degree_name' => $request->input('degree_name'),
                'subject' => $request->input('subject'),
                'result' => $request->input('result'),
                'status' => $request->input('status'),
                'created_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
            );

            DB::table('education')->insert($data);

            $notification = array(
                'message' => 'Education Information Added Successfully!',
                'alert-type' => 'success'
            );
            return back()->with($notification);
        } elseif ($param == 'do_update') {
            $this->validate($request, [
                'session' => 'required',
                'institute_name' => 'required',
                'degree_name' => 'required',
                'subject' => 'required',
                'result' => 'required',
                'status' => 'required',
            ]);

            $data = array(
                'session' => $request->input('session'),
                'institute_name' => $request->input('institute_name'),
                'degree_name' => $request->input('degree_name'),
                'subject' => $request->input('subject'),
                'result' => $request->input('result'),
                'status' => $request->input('status'),
                'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
            );

            $id = $request->input('education_id');

            DB::table('education')->where('education_id', $id)->update($data);

            $notification = array(
                'message' => 'Education Information Updated Successfully!',
                'alert-type' => 'success'
            );

            return back()->with($notification);
        } elseif ($param == 'delete') {
            DB::table('education')->where('education_id', $param2)->delete();

            $notification = array(
                'message' => 'Education Information Deleted Successfully!',
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }

        $data = array();
        $data['page_name'] = 'Education';
        $data['account_type'] = 'admin';
        $page = view('backend.admin.education', $data);

        return view('backend.master', $data)->with('page', $page);

    }

    public function workplace(Request $request, $param = NULL, $param2=NULL)
    {
        
        if ($param == 'create') {
            $this->validate($request, [
                'duration' => 'required',
                'company_name' => 'required',
                'designation' => 'required',
                'language' => 'required',
                'description' => 'required',
                'status' => 'required',
            ]);

            $data = array(
                'duration' => $request->input('duration'),
                'company_name' => $request->input('company_name'),
                'designation' => $request->input('designation'),
                'language' => $request->input('language'),
                'description' => $request->input('description'),
                'status' => $request->input('status'),
                'created_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
            );

            DB::table('workplace')->insert($data);

            $notification = array(
                'message' => 'Workplace Information Added Successfully!',
                'alert-type' => 'success'
            );
            return back()->with($notification);
        } elseif ($param == 'do_update') {
            $this->validate($request, [
                'duration' => 'required',
                'company_name' => 'required',
                'designation' => 'required',
                'language' => 'required',
                'description' => 'required',
                'status' => 'required',
            ]);

            $data = array(
                'duration' => $request->input('duration'),
                'company_name' => $request->input('company_name'),
                'designation' => $request->input('designation'),
                'language' => $request->input('language'),
                'description' => $request->input('description'),
                'status' => $request->input('status'),
                'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
            );

            $id = $request->input('workplace_id');

            DB::table('workplace')->where('workplace_id', $id)->update($data);

            $notification = array(
                'message' => 'Workplace Information Updated Successfully!',
                'alert-type' => 'success'
            );

            return back()->with($notification);
        } elseif ($param == 'delete') {
            DB::table('workplace')->where('workplace_id', $param2)->delete();

            $notification = array(
                'message' => 'Workplace Information Deleted Successfully!',
                'alert-type' => 'error'
            );
            return back()->with($notification);
        }

        $data = array();
        $data['page_name'] = 'Education';
        $data['account_type'] = 'admin';
        $page = view('backend.admin.workplace', $data);

        return view('backend.master', $data)->with('page', $page);

    }

    public function services(Request $request, $param = NULL)
    {
    

    }


    public function projects(Request $request, $param = NULL)
    {
        

        if ($param == 'create') {
            $this->validate($request, [
                'title' => 'required',
                'description' => 'required',
                'featured_image' => 'required',
                'tags' => 'required',
            ]);

            //Getting image file and setting the name of image with extension
            $photo = $request->file('featured_image');
            $image_name = time().'.'.$photo->getClientOriginalExtension();

            //Setting up the image path
            $destinationPath = public_path('/uploads/projects/');
            $photo->move($destinationPath, $image_name);

            $img = Image::make('public/uploads/projects/'.$image_name);
            //Image resizing with aspect ratio and saving it to the directory
            $img->resize(null, 390,function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->save($destinationPath.'/'.$image_name,70);
            $image_path = 'public/uploads/projects/'.$image_name;


            $data = array(
                'title' => $request->input('title'),
                'description' => $request->input('description'),
                'demo_link' => $request->input('demo_link'),
                'tags' => $request->input('tags'),
                'status' => $request->input('status'),
                'featured_image' => $image_path,
                'created_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString(),
                'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
            );

            DB::table('projects')->insert($data);

            $notification = array(
                'message' => 'Project Information saved Successfully!',
                'alert-type' => 'success'
            );
            return back()->with($notification);
        } elseif ($param == 'do_update') {
            $this->validate($request, [
                'title' => 'required',
                'description' => 'required',
                'tags' => 'required',
            ]);

            if ($request->featured_image != NULL) {


                //Getting image file and setting the name of image with extension
                $photo = $request->file('featured_image');
                $image_name = time().'.'.$photo->getClientOriginalExtension();

                //Setting up the image path
                $destinationPath = public_path('/uploads/projects/');
                $photo->move($destinationPath, $image_name);

                $img = Image::make('public/uploads/projects/'.$image_name);
                //Image resizing with aspect ratio and saving it to the directory
                $img->resize(null, 390,function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->save($destinationPath.'/'.$image_name,70);
                $image_path = 'public/uploads/projects/'.$image_name;


                $data = array(
                    'title' => $request->input('title'),
                    'description' => $request->input('description'),
                    'demo_link' => $request->input('demo_link'),
                    'tags' => $request->input('tags'),
                    'status' => $request->input('status'),
                    'featured_image' => $image_path,
                    'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
                );

                $id = $request->input('project_id');


                DB::table('projects')->where('project_id', $id)->update($data);
                $notification = array(
                    'message' => 'Project\'s Information Updated Successfully!',
                    'alert-type' => 'success'
                );
                return back()->with($notification);
            } else {

                $data = array(
                    'title' => $request->input('title'),
                    'description' => $request->input('description'),
                    'demo_link' => $request->input('demo_link'),
                    'tags' => $request->input('tags'),
                    'status' => $request->input('status'),
                    'updated_at' => \Carbon\Carbon::now()->addHours(6)->toDateTimeString()
                );

                $id = $request->input('project_id');
                DB::table('projects')->where('project_id', $id)->update($data);
            }
            $notification = array(
                'message' => 'Project\'s Information Updated Successfully!',
                'alert-type' => 'success'
            );
            return back()->with($notification);
        } elseif ($param == 'delete') {

            DB::table('projects')->where('article_id', $param)->delete();

            $notification = array(
                'message' => 'Project\'s Information Deleted Successfully!',
                'alert-type' => 'success'
            );
            return back()->with($notification);
        }

        $data = array();
        $data['page_name'] = 'Projects';
        $data['account_type'] = 'admin';
        $page = view('backend.admin.projects', $data);

        return view('backend.master', $data)->with('page', $page);
    }

    function projectById($id)
    {

        $data = DB::table('projects')->where('project_id',$id)->first();

        echo json_encode($data);
    }


    public function contact(Request $request, $param = NULL)
    {
    

    }

}
