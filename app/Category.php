<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
	protected $fillable = [
		'category_name', 'category_description', 'status'
	];
	
	public function articles()
	{
		return $this->hasMany(Article::class, 'category','category_id');
	}
}
