<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'home', 'uses' => 'webController@index']);




Route::get('/blog/project-gallery/{id}/{title}', ['as' => 'project.details', 'uses' => 'webController@projectGallery']);

/*
 * BLOG ROUTE
 */
Route::get('/blog', ['as' => 'blog', 'uses' => 'Blogs@index']);
Route::get('/blog/article/{id}/{title}', ['as' => 'blog.article', 'uses' => 'Blogs@article']);

Route::get('/blog/category/{id}/{title}', ['as' => 'blog.category', 'uses' => 'Blogs@category']);

Route::post('/blog/contact', ['as' => 'mail.contact', 'uses' => 'Blogs@contact']);

Route::post('/blog/subscribe', ['as' => 'subscribe', 'uses' => 'Blogs@subscribe']);

Route::get('blog/search-queries', ['as' => 'queries', 'uses' => 'Blogs@search']);

Route::get('/archives', ['as' => 'archives', 'uses' => 'Blogs@archives']);

Route::get('blog/tag/{tag}', ['as' => 'article.tag', 'uses' => 'Blogs@getArticleByTag']);


/*
 * ! Blog Route
 */

/*
 * Login Routes
 */

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('/user/dashboard',function (){
	echo 'Welcome to user dashboard!<br/>We will update user dashboard within few days. Thank for your registration!
	<a href="'.route('logout').'">Logout</a>';
});



/*
 * SOCIAL LOGIN WITH FACEBOOK / TWITTER / GMAIL
 */


$s = 'social.';
Route::get('/login/{provider}/redirect',   ['as' => $s . 'redirect',   'uses' => 'SocialLoginController@getSocialRedirect']);
Route::get('/login/{provider}/callback',     ['as' => $s . 'handle',     'uses' => 'SocialLoginController@getSocialHandle']);




/*
 * Admin Routes
 */

Route::group(['middleware' => ['auth','AdminRoutes'] ], function () {
	
	
	Route::get('/admin_dashboard', ['as' => 'admin_dashboard', 'uses' => 'Admin@index']);
	Route::get('/admin_dashboard/profile', ['as' => 'admin.profile', 'uses' => 'Admin@profile']);
	Route::post('/admin_dashboard/profile/{image}', ['as' => 'profile.image', 'uses' => 'Admin@profile']);
	Route::post('/admin_dashboard/profile/{do_update}', ['as' => 'profile.update', 'uses' => 'Admin@profile']);
	
	
	/*
	 * Category CRUD
	 */
	
	Route::get('/admin_dashboard/categories', ['as' => 'categories', 'uses' => 'Admin@categories']);
	Route::post('/admin_dashboard/categories/{create}', ['as' => 'categories.create', 'uses' => 'Admin@categories']);
	Route::post('/admin_dashboard/categories/{do_update}/', ['as' => 'categories.update', 'uses' => 'Admin@categories']);
	Route::get('/admin_dashboard/categories/{delete}/{id}', ['as' => 'categories.delete', 'uses' => 'Admin@categories']);
	
	
	/*
	 * ARTICLE CRUD
	 */
	
	Route::get('/admin_dashboard/articles', ['as' => 'articles', 'uses' => 'Admin@articles']);
	Route::post('/admin_dashboard/articles/{create}', ['as' => 'articles.create', 'uses' => 'Admin@articles']);
	Route::post('/admin_dashboard/articles/{do_update}/', ['as' => 'articles.update', 'uses' => 'Admin@articles']);
	Route::get('/admin_dashboard/articles/{delete}/{id}', ['as' => 'articles.delete', 'uses' => 'Admin@articles']);
	
	Route::get('/admin_dashboard/articles/{id}', ['as' => 'articles.edit', 'uses' => 'Admin@articleById']);
	
	
	/*
	 * GALLERY
	 */
	Route::get('/admin_dashboard/uploaded_image', ['as' => 'gallery', 'uses' => 'Admin@uploaded_image']);
	
	
	
	/*
	 * Portfolio Front End Manage
	 */
	
	
	Route::get('/admin_dashboard/banner', ['as' => 'banner', 'uses' => 'FrontendManagement@index']);
	Route::post('/admin_dashboard/banner/{do_update}', ['as' => 'banner.update', 'uses' => 'FrontendManagement@index']);
	
	Route::get('/admin_dashboard/about', ['as' => 'about', 'uses' => 'FrontendManagement@about']);
	Route::post('/admin_dashboard/about/{do_update}', ['as' => 'about.update', 'uses' => 'FrontendManagement@about']);
	
	Route::post('/admin_dashboard/education/', ['as' => 'education', 'uses' => 'FrontendManagement@education']);
	Route::post('/admin_dashboard/education/{create}', ['as' => 'education.create', 'uses' => 'FrontendManagement@education']);
	Route::post('/admin_dashboard/education/{delete}/{id}', ['as' => 'education.update', 'uses' => 'FrontendManagement@education']);
	
	Route::get('/admin_dashboard/workplace/', ['as' => 'workplace', 'uses' => 'FrontendManagement@workplace']);
	Route::post('/admin_dashboard/workplace/{create}', ['as' => 'workplace.create', 'uses' => 'FrontendManagement@workplace']);
	Route::get('/admin_dashboard/workplace/{delete}/{id}', ['as' => 'workplace.update', 'uses' => 'FrontendManagement@workplace']);
	
	
	Route::get('/admin_dashboard/projects', ['as' => 'projects', 'uses' => 'FrontendManagement@projects']);
	
	Route::post('/admin_dashboard/projects/{create}', ['as' => 'projects.create', 'uses' => 'FrontendManagement@projects']);
	Route::post('/admin_dashboard/projects/{do_update}/', ['as' => 'projects.update', 'uses' => 'FrontendManagement@projects']);
	Route::get('/admin_dashboard/projects/{delete}/{id}', ['as' => 'projects.delete', 'uses' => 'FrontendManagement@projects']);
	
	Route::get('/admin_dashboard/projects/{id}', ['as' => 'projects.edit', 'uses' => 'FrontendManagement@projectById']);
	
	
	Route::get('/admin_dashboard/skill_set', ['as' => 'skill.set', 'uses' => 'FrontendManagement@skill_set']);
	Route::get('/admin_dashboard/experience', ['as' => 'experience', 'uses' => 'FrontendManagement@experience']);
	Route::get('/admin_dashboard/education', ['as' => 'education', 'uses' => 'FrontendManagement@education']);
	Route::get('/admin_dashboard/services', ['as' => 'services', 'uses' => 'FrontendManagement@services']);
	
	Route::get('/admin_dashboard/contact', ['as' => 'contact', 'uses' => 'FrontendManagement@contact']);
	
	
	
});

