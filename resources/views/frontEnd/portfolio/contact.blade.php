<div class="contact" id="contact">
    <h3 class="w3_head w3_head1">Contact Me <span>Get in touch with me</span></h3>
    <div class="col-md-5 contact-agileits-w3layouts">
        @if (count($errors) > 0)
            <div>
                <ul>
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">
                            {{ $error }}
                        </div>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ route('mail.contact') }}"  method="post">
            {{ csrf_field() }}
            <input type="text" name="name" placeholder="Name" required="">
            <input type="email" name="email" placeholder="Email" required="">
            <input type="text" name="contact" placeholder="Optional">
            <textarea name="message" placeholder="Message" required=""></textarea>
            <input type="submit" value="Submit">
        </form>
    </div>


    <div class="col-md-7 contact-map-right">
        <div id="map"></div>
        <!-- Map-JavaScript -->
        {{--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>--}}
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA28Bsqv5UtUvZIe5Wvyblka2up1FYVcts"></script>
        <script type="text/javascript">
            google.maps.event.addDomListener(window, 'load', init);
            function init() {
                var mapOptions = {
                    zoom: 11,
                    center: new google.maps.LatLng(23.8103, 90.4125),
                    styles: [{
                        "featureType": "all",
                        "elementType": "labels.text.fill",
                        "stylers": [{"saturation": 36}, {"color": "#000000"}, {"lightness": 40}]
                    }, {
                        "featureType": "all",
                        "elementType": "labels.text.stroke",
                        "stylers": [{"visibility": "on"}, {"color": "#000000"}, {"lightness": 16}]
                    }, {
                        "featureType": "all",
                        "elementType": "labels.icon",
                        "stylers": [{"visibility": "off"}]
                    }, {
                        "featureType": "administrative",
                        "elementType": "geometry.fill",
                        "stylers": [{"color": "#000000"}, {"lightness": 20}]
                    }, {
                        "featureType": "administrative",
                        "elementType": "geometry.stroke",
                        "stylers": [{"color": "#000000"}, {"lightness": 17}, {"weight": 1.2}]
                    }, {
                        "featureType": "landscape",
                        "elementType": "geometry",
                        "stylers": [{"color": "#000000"}, {"lightness": 20}]
                    }, {
                        "featureType": "poi",
                        "elementType": "geometry",
                        "stylers": [{"color": "#000000"}, {"lightness": 21}]
                    }, {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [{"color": "#000000"}, {"lightness": 17}]
                    }, {
                        "featureType": "road.highway",
                        "elementType": "geometry.stroke",
                        "stylers": [{"color": "#000000"}, {"lightness": 29}, {"weight": 0.2}]
                    }, {
                        "featureType": "road.arterial",
                        "elementType": "geometry",
                        "stylers": [{"color": "#000000"}, {"lightness": 18}]
                    }, {
                        "featureType": "road.local",
                        "elementType": "geometry",
                        "stylers": [{"color": "#000000"}, {"lightness": 16}]
                    }, {
                        "featureType": "transit",
                        "elementType": "geometry",
                        "stylers": [{"color": "#000000"}, {"lightness": 19}]
                    }, {
                        "featureType": "water",
                        "elementType": "geometry",
                        "stylers": [{"color": "#000000"}, {"lightness": 17}]
                    }]
                };
                var mapElement = document.getElementById('map');
                var map = new google.maps.Map(mapElement, mapOptions);
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(23.8103, 90.4125),
                    map: map,
                });
            }
        </script>
        <!-- //Map-JavaScript -->

    </div>
    <div class="clearfix"></div>
</div>