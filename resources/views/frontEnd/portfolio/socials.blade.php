<div class="contact-info">
    <div class="container">
        <div class="col-md-4 contact-grids">
            <h5>Find me</h5>
            <div class="w3_agileits_social_media-footer">
                <ul>
                    <?php $facebook = DB::table('banner_content')->where('info_type', 'facebook')->value('description'); ?>
                    <?php $twitter = DB::table('banner_content')->where('info_type', 'twitter')->value('description'); ?>
                    <?php $bitbucket = DB::table('banner_content')->where('info_type', 'bitbucket')->value('description'); ?>
                    <?php $stack_overflow = DB::table('banner_content')->where('info_type', 'stack_overflow')->value('description'); ?>

                    <li><a href="{{$facebook}}" target="_blank" class="wthree_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                    <li><a href="{{ $twitter }}" target="_blank" class="wthree_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                    <li><a href="{{ $bitbucket }}" target="_blank" class="wthree_dribbble"><i class="fa fa-bitbucket" aria-hidden="true"></i></a></li>
                    <li><a href="{{ $stack_overflow }}" target="_blank" class="wthree_behance"><i class="fa fa-stack-overflow" aria-hidden="true"></i></a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-4 contact-grids contact-mid">
            <h5>Contact Info</h5>
            <ul>
                <?php $phone = DB::table('about_content')->where('info_type', 'phone')->value('description'); ?>
                <li><i class="fa fa-phone" aria-hidden="true"></i>{{ $phone }}</li>
                 <?php $address = DB::table('about_content')->where('info_type', 'address')->value('description'); ?>
                <li><i class="fa fa-map-marker" aria-hidden="true"></i>{{ $address }}</li>
                <?php $email = DB::table('about_content')->where('info_type', 'email')->value('description'); ?>
                <li><i class="fa fa-envelope-o" aria-hidden="true"></i><a href="mailto:{{$email}}">{{$email}}</a></li>
            </ul>
        </div>
        <div class="col-md-4 contact-grids">
            <h5>My Twitter</h5>
            <div class="callbacks_container">
                <ul class="rslides" id="slider3">
                    <li>
                        <i class="fa fa-twitter" aria-hidden="true"></i>Praesent imperdiet diam sagittis, egestas <a
                                href="#" class="mail">
                            @http://t.co/9vslJFpW</a> <span>ABOUT 15 MINS</span>
                    </li>
                    <li>
                        <i class="fa fa-twitter" aria-hidden="true"></i>Diam sagittis, egestas praesent imperdiet<a
                                href="#" class="mail">
                            @http://t.co/9vslJFpW</a> <span>ABOUT 2 HOURS</span>
                    </li>
                    <li>
                        <i class="fa fa-twitter" aria-hidden="true"></i>Imperdiet diam sagittis, praesent egestas <a
                                href="#" class="mail">
                            @http://t.co/9vslJFpW</a> <span>ABOUT 3 DAYS</span>
                    </li>
                    <li>
                        <i class="fa fa-twitter" aria-hidden="true"></i>Egestas praesent imperdiet diam sagittis <a
                                href="#" class="mail">
                            @http://t.co/9vslJFpW</a> <span>ABOUT 1 WEEK</span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>