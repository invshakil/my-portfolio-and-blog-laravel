<div class="banner" id="home">
    <div class="header">
        <div class="w3_agile_logo">
            <h1><a href="{{ url('/') }}">PORTFOLIO<span>SHAKIL</span></a></h1>
        </div>
        <div class="w3_menu">
            <div class="mobile-nav-button">
                <div class="mobile-nav-button__line"></div>
                <div class="mobile-nav-button__line"></div>
                <div class="mobile-nav-button__line"></div>
            </div>
            <nav class="mobile-menu">
                <ul>
                    <li><a href="#home" class="active scroll">Home</a></li>
                    <li><a href="{{ url('/blog') }}">Blog</a></li>
                    <li><a href="#about" class="scroll">About</a></li>
                    <li><a href="#skills" class="scroll">Skills</a></li>
                    <li><a href="#work" class="scroll">Education</a></li>
                    <li><a href="#gallery" class="scroll">Gallery</a></li>
                    <li><a href="#contact" class="scroll">Contact</a></li>
                </ul>
                <div class="clearfix"></div>
                <div class="search-agileits">
                    <form action="#" method="post">
                        <input type="search" name="Search" placeholder=" " required="">
                        <input type="submit" value="Search">
                    </form>
                </div>
            </nav>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="agile_banner_info">
        <?php $name = DB::table('banner_content')->where('info_type', 'name')->value('description'); ?>
        <h2><span>HI! I'm</span> {{ $name }}</h2>
        <div id="typed-strings" class="agileits_w3layouts_strings">
            <?php $designation = DB::table('banner_content')->where('info_type', 'designation')->value('description');
            $des = explode(',', $designation);
            ?>
            @foreach($des as $d)
                <p>{{ $d }}</p>
            @endforeach

        </div>
        <span id="typed" style="white-space:pre;"></span>
            <?php $intro = DB::table('banner_content')->where('info_type', 'intro_text')->value('description'); ?>
        <p class="banner-p-w3ls">{{ $intro }}</p>
    </div>
    <div class="w3_agileits_social_media">
        <ul>
            <?php $facebook = DB::table('banner_content')->where('info_type', 'facebook')->value('description'); ?>
            <?php $twitter = DB::table('banner_content')->where('info_type', 'twitter')->value('description'); ?>
            <?php $bitbucket = DB::table('banner_content')->where('info_type', 'bitbucket')->value('description'); ?>
            <?php $stack_overflow = DB::table('banner_content')->where('info_type', 'stack_overflow')->value('description'); ?>

            <li><a href="{{$facebook}}" target="_blank" class="wthree_facebook"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="{{ $twitter }}" target="_blank" class="wthree_twitter"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            <li><a href="{{ $bitbucket }}" target="_blank" class="wthree_dribbble"><i class="fa fa-bitbucket" aria-hidden="true"></i></a></li>
            <li><a href="{{ $stack_overflow }}" target="_blank" class="wthree_behance"><i class="fa fa-stack-overflow" aria-hidden="true"></i></a></li>
        </ul>
    </div>

    <?php $image = DB::table('banner_content')->where('info_type', 'image')->value('description'); ?>
    <div class="banner-image-w3layouts" style="padding-top: -50px">
        <img src="{{ url($image) }}" alt=" " class="img-responsive img-rounded"/>
    </div>
</div>