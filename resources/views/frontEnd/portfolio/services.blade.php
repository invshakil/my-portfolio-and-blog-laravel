<div class="services" id="classes">
    <div class="container">
        <h3 class="w3_head w3_head1">My Services <span>What I can do for you</span></h3>
    </div>
    <div class="agileits_services_grids_bottom">
        <div class="container">
            <div class="col-md-4 agileits_service_grid_btm_left">
                <div class="agileits_service_grid_btm_left1">
                    <div class="agileits_service_grid_btm_left2">
                        <i class="fa fa-desktop" aria-hidden="true"></i>
                        <h5>Software Development</h5>
                        <p>I can develop any software as per requirement with client satisfaction. I will work with you as hourly/fixed price agreement.</p>
                    </div>
                    <img src="{{ url('public') }}/portfolio/images/4.jpg" alt=" " class="img-responsive"/>
                </div>
            </div>
            <div class="col-md-4 agileits_service_grid_btm_left">
                <div class="agileits_service_grid_btm_left1">
                    <div class="agileits_service_grid_btm_left2">
                        <i class="fa fa-cogs" aria-hidden="true"></i>
                        <h5>Software Maintenance</h5>
                        <p>I can do maintenance work for your existing system as long as you want. But that will require hourly contract agreement.</p>
                    </div>
                    <img src="{{ url('public') }}/portfolio/images/5.jpg" alt=" " class="img-responsive"/>
                </div>
            </div>
            <div class="col-md-4 agileits_service_grid_btm_left">
                <div class="agileits_service_grid_btm_left1">
                    <div class="agileits_service_grid_btm_left2">
                        <i class="fa fa-briefcase" aria-hidden="true"></i>
                        <h5>Software Testing</h5>
                        <p>I can do basic software testing for your existing system. Will sort out the bugs and will fix it for you if you want.</p>
                    </div>
                    <img src="{{ url('public') }}/portfolio/images/6.jpg" alt=" " class="img-responsive"/>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>