<div class="gallery_wthree" id="gallery" style="background-color: #95a5a6">
    <div class="container">
        <h3 class="w3_head w3_head1">My Projects <span>My latest work</span></h3>
        <div class="gallery_grid agileits_w3layouts">
            <div class="col-md-12  col-sm-6 col-xs-12 grid_w3">

                @php
                    $projects = DB::table('projects')->where('status',1)->get();
                @endphp

                @foreach($projects as $item)
                    <div class="sub_grid grid-1 col-md-4 col-sm-6" style="padding-bottom: 25px;">
                        <a href="{{ url('/blog/project-gallery/'.$item->project_id.'/'.str_replace(' ','-',$item->title)) }}">
                            <img src="{{ url($item->featured_image) }}" alt="{{ $item->title }}" class="img-responsive"/>
                            <div class="w3agile-text w3agile-text-smal1">
                                <h5>{{ $item->title }}</h5>
                            </div>
                        </a>
                    </div>
                @endforeach

            </div>


            <div class="clearfix"></div>
        </div>
    </div>
</div>