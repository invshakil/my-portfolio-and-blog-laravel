<div class="services" id="classes" style="background-color: #7f7f7f;">
    <div class="container">
        <h3 class="w3_head w3_head1">My Recent Blogs <span>My writings</span></h3>
    </div>
    <div class="agileits_services_grids_bottom">
        <div class="container">
            @php
                $result = DB::table('articles')->limit(3)->orderBy('article_id','desc')
                                    ->join('categories', 'articles.category', '=', 'categories.category_id')
                                    ->join('users', 'articles.author', '=', 'users.id')
                                    ->select('articles.*', 'users.name', 'categories.category_name')
                                    ->get();
            @endphp
            @foreach($result as $item)
            <div class="col-md-4 agileits_service_grid_btm_left">
                <div class="agileits_service_grid_btm_left1">
                    <div class="agileits_service_grid_btm_left2">
                        <h5><a href="{{ url('/blog/article'.'/'.$item->article_id.'/'.str_replace(' ','-',$item->title) ) }}">{{ $item->title }}</a></h5>
                        <a class="btn btn-primary btn-sm" href="{{ url('/blog/article'.'/'.$item->article_id.'/'.str_replace(' ','-',$item->title) ) }}">Read This Article</a>
                    </div>
                    <a href="{{ url('/blog/article'.'/'.$item->article_id.'/'.str_replace(' ','-',$item->title) ) }}"><img src="{{ url($item->image) }}" alt=" " height="100" class="img-responsive"/></a>
                </div>
            </div>
            @endforeach

            <div class="clearfix"></div>
        </div>
    </div>
</div>