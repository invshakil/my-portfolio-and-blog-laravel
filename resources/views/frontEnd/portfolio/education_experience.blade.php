<div class="work" id="work">
    <div class="container">
        <h3 class="w3_head w3_head1">experience & Education <span>More about my past</span></h3>
        <h5>Work experience</h5>

        @php $workplace = DB::table('workplace')->orderBy('workplace_id','desc')->get(); @endphp
        @foreach($workplace as $item)
        <div class="work-info-agileits">
            <div class="col-md-4 work-left-agileits-w3layouts">
                <h6><i class="fa fa-calendar-check-o" aria-hidden="true"></i> {{ $item->duration }}</h6>
            </div>
            <div class="col-md-8 work-right-w3-agileits">
                <h3>{{$item->company_name}} / <span>{{ $item->designation }}</span></h3>
                <p><b><u>Language/Platform</u>:</b> {{ $item->language }}</p><br/>
                <p>{{ $item->description }}</p>
            </div>
            <div class="clearfix"></div>
        </div>
        @endforeach

        <h5 class="work2">Academic Qualification</h5>

        @php $education = DB::table('education')->orderBy('education_id','desc')->get(); @endphp

        @foreach($education as $item)
        <div class="work-info-agileits">
            <div class="col-md-4 work-left-agileits-w3layouts">
                <h6><i class="fa fa-calendar-check-o" aria-hidden="true"></i> {{ $item->session }}</h6>
            </div>
            <div class="col-md-8 work-right-w3-agileits">
                <h3>{{ $item->institute_name }}</h3>
                <p> <b>Degree: </b>{{$item->degree_name}} </p>
                <p> <b>Group/Subject: </b>{{$item->subject}} </p>
                <p> <b>Result: </b>{{$item->result}} </p>
            </div>
            <div class="clearfix"></div>
        </div>
        @endforeach

    </div>
</div>