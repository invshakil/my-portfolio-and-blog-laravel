<div class="skills-agileinfo" id="skills">
    <div class="container">
        <h3 class="w3_head w3_head1 white-agile">My Skills <span>My special expertise</span></h3>
        <div class="modal-spa">
            <div class="skills">
                <div class="col-md-6 bar-grids bargrids-left">
                    <h4><span class="glyphicon glyphicon-cog"></span> Technical Skills </h4>
                    <h6>Codeigniter (PHP) <span> 80% </span></h6>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" style="width: 80%">
                        </div>
                    </div>
                    <h6>Laravel (PHP) <span> 70% </span></h6>
                    <div class="progress  w3-agile">
                        <div class="progress-bar progress-bar-striped active" style="width: 70%">
                        </div>
                    </div>
                    <h6>Javascript & jQuery<span>70% </span></h6>
                    <div class="progress  w3-agile">
                        <div class="progress-bar progress-bar-striped active" style="width: 70%">
                        </div>
                    </div>
                    <h6>Git (Version Control)<span>70% </span></h6>
                    <div class="progress  w3-agile">
                        <div class="progress-bar progress-bar-striped active" style="width: 70%">
                        </div>
                    </div>
                    <h6>Bootstrap <span> 90% </span></h6>
                    <div class="progress  w3-agile prgs-w3agile-last">
                        <div class="progress-bar progress-bar-striped active" style="width: 75%">
                        </div>
                    </div>
                </div>

                <div class="col-md-6 bar-grids  w3-agile">
                    <h4><span class="glyphicon glyphicon-briefcase"></span> Professional Skills </h4>
                    <h6>Communication<span> 85% </span></h6>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" style="width: 85%">
                        </div>
                    </div>
                    <h6>Ability to Work Under Pressure<span> 90% </span></h6>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" style="width: 90%">
                        </div>
                    </div>
                    <h6>Time Management<span> 85% </span></h6>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" style="width: 85%">
                        </div>
                    </div>
                    <h6>Leadership<span>85% </span></h6>
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped active" style="width: 85%">
                        </div>
                    </div>
                    <h6>Confidence<span>90% </span></h6>
                    <div class="progress prgs-w3agile-last">
                        <div class="progress-bar progress-bar-striped active" style="width: 90%">
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>