<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Portfolio of Shakil</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="keywords" content="Saif Shakil, Inverse Shakil, Md. Syful Islam Shakil, Software Engineer, Freelancer"/>
    <script type="application/x-javascript"> addEventListener("load", function () {
            setTimeout(hideURLbar, 0);
        }, false);
        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <link href="{{ url('public') }}/portfolio/css/bootstrap.css" rel="stylesheet" type="text/css" media="all"/>
    <link href="{{ url('public') }}/portfolio/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ url('public') }}/portfolio/css/lightbox.css" type="text/css" media="all"/>
    <!--gallery-popup-css-->
    <link href="{{ url('public') }}/portfolio/css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>
    <!-- Pop-up -->
    <link href="{{ url('public') }}/portfolio/css/style.css" rel="stylesheet" type="text/css" media="all"/>
    <!--fonts-->
    <link href="//fonts.googleapis.com/css?family=Josefin+Sans:100,300,400,600,700" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">
    <!--//fonts-->

    <link rel="shortcut icon" href="{{ url('public') }}/favicon.ico" type="image/x-icon">

</head>
<body>
<!-- banner -->
@include('frontEnd.portfolio.banner')
<!-- //banner -->
<!-- about -->
@include('frontEnd.portfolio.about')

<!-- //about-bottom -->

@include('frontEnd.portfolio.blogs')
<!-- mid -->

<!-- //mid -->
<!-- gallery -->
@include('frontEnd.portfolio.projects')
<!-- //gallery -->
<!--skills -->
@include('frontEnd.portfolio.skill')
<!-- //skills -->
<!-- experiences -->

@include('frontEnd.portfolio.education_experience')
<!-- //experiences -->
<!-- services -->
@include('frontEnd.portfolio.services')
<!-- //services -->


<!--footer-->
@include('frontEnd.portfolio.contact')

@include('frontEnd.portfolio.socials')


<div class="copy-w3-agile">
    <p>Copyright 2017 All Rights Reserved | Developed By <a href="https://www.sshakil.com/">Saif Shakil</a></p>
</div>
<!--//footer-->

<!-- js -->
@include('frontEnd.portfolio.footerscript')

</body>
</html>