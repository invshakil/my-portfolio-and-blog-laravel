<div class="about" id="about">
    <div class="col-md-4 col-sm-5 col-xs-12">
		<?php $image = DB ::table('about_content') -> where('info_type', 'image') -> value('description'); ?>

        <img src="{{ url($image) }}" class="img-responsive img-rounded" width="100%">

    </div>
    <div class="col-md-8 col-sm-7 col-xs-12 person-info-agileits-w3layouts">
		<?php $name = DB ::table('about_content') -> where('info_type', 'name') -> value('description'); ?>
        <h3 class="w3_head w3_head1">About Me <span>I am {{ $name }}</span></h3>
		<?php $about = DB ::table('about_content') -> where('info_type', 'about') -> value('description'); ?>
        <p>{!! $about !!}</p>
        <ul>
            <li><span>Name</span>: {{ $name }}</li>

			<?php $age = DB ::table('about_content') -> where('info_type', 'age') -> value('description'); ?>
            <li><span>Age</span>: {{ $age }}</li>

			<?php $experience = DB ::table('about_content') -> where('info_type', 'experience') -> value('description'); ?>
            <li><span>Experience</span>: {{$experience}}</li>

			<?php $email = DB ::table('about_content') -> where('info_type', 'email') -> value('description'); ?>
            <li><span>Email</span>: <a href="mailto:{{$email}}">{{$email}}</a></li>

			<?php $phone = DB ::table('about_content') -> where('info_type', 'phone') -> value('description'); ?>
            <li><span>Phone</span>: {{$phone}}</li>

			<?php $website = DB ::table('about_content') -> where('info_type', 'website') -> value('description'); ?>
            <li><span>Website</span>: <a href="{{$website}}" target="_blank">{{$website}}</a></li>

			<?php $address = DB ::table('about_content') -> where('info_type', 'address') -> value('description'); ?>
            <li><span>Address</span>: {{$address}}</li>
            <li>
				<?php $cv = DB ::table('about_content') -> where('info_type', 'cv') -> value('description'); ?>
                <a href="{{ $cv }}" target="_blank" class="botton-w3ls">Check CV</a>

				<?php $blog = DB ::table('about_content') -> where('info_type', 'blog') -> value('description'); ?>
                <a href="{{ $blog }}" target="_blank" class="botton-w3ls">Check Out My Blog</a>

                {{--<a href="#small-dialog" class="play-icon popup-with-zoom-anim botton-w3ls">Watch my video</a>--}}
                <a href="#" data-toggle="modal" data-target="#myModal"
                   class="botton-w3ls">Watch my video</a>

				<?php $fb = DB ::table('banner_content') -> where('info_type', 'facebook') -> value('description'); ?>
                <a href="{{$fb}}" target="_blank" class="botton-w3ls">Follow me on Facebook <i
                            class="fa fa-facebook btn btn-primary btn-sm" aria-hidden="true"></i></a>
            </li>
        </ul>
    </div>
</div>

{{--<div id="small-dialog" class="mfp-hide w3ls_small_dialog wthree_pop">--}}
{{--<div class="agileits_modal_body">--}}

{{--<iframe src="{{$video}}"></iframe>--}}
{{--</div>--}}
{{--</div>--}}

<section class="mid-w3layouts">
    <div class="container">
        <div class="col-md-12">
            <h6>Iam available for freelance projects.</h6>
            <h3>Let's <span>work</span> together indeed!</h3>
            <a href="#contact" class="scroll">Get quotes</a>
        </div>
    </div>

</section>


<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">My Latest Tutorial</h4>
            </div>
            <div class="modal-body">

                <?php $video = DB ::table('about_content') -> where('info_type', 'video') -> value('description');
				$y_id = explode('v=', $video);
				$link = $y_id[1];
				?>

                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/{{$link}}" frameborder="0"
                            gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>