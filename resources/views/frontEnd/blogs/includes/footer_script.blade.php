
<script src="{{ url('public/blog') }}/assets/js/wow.min.js"></script>
<script src="{{ url('public/blog') }}/assets/js/bootstrap.min.js"></script>
<script src="{{ url('public/blog') }}/assets/js/slick.min.js"></script>
<script src="{{ url('public/blog') }}/assets/js/jquery.li-scroller.1.0.js"></script>
<script src="{{ url('public/blog') }}/assets/js/custom.js"></script>

<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>
            @if(Session::has('message'))
    var type = "{{Session::get('alert-type','info')}}";


    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif
</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-44033160-3"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-44033160-3');
</script>
