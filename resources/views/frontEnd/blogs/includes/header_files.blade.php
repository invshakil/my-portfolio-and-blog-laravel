<head>
    <title>{{ $page_title }}</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ url('public/blog') }}/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" />
    <link rel="stylesheet" type="text/css" href="{{ url('public/blog') }}/assets/css/animate.css">
    <link rel="stylesheet" type="text/css" href="{{ url('public/blog') }}/assets/css/li-scroller.css">
    <link rel="stylesheet" type="text/css" href="{{ url('public/blog') }}/assets/css/slick.css">
    <link rel="stylesheet" type="text/css" href="{{ url('public/blog') }}/assets/css/theme.css">
    <link rel="stylesheet" type="text/css" href="{{ url('public/blog') }}/assets/css/style.css">

    <link rel="shortcut icon" href="{{ url('public') }}/favicon.ico" type="image/x-icon">

    <script src="{{ url('public/blog') }}/assets/js/jquery.min.js"></script>

    <!-- META INFORMATION -->
    <meta property="og:locale" content="en_US" />
    <meta property="fb:app_id" content="1302445676533234"/>
    <meta property="og:site_name" content="Shakil's Blog"/>
    <meta property="og:url" content="{{url()->current()}}"/>
    <meta property="og:title" content="{{ $page_title }}"/>

    @if(isset($image))
        <meta property="og:image" content="{{ url($image) }}"/>
    @endif

    @if(isset($description))
        <meta property="og:description" content="{{ $description }}"/>
        <meta name="description" content="{{ $description }}"/>
    @endif

    @if(isset($keywords))

        <meta name="keyword" content="{{ $keywords }}"/>
    @endif

    @if(isset($author))
        <meta property="og:type" content="article" />
        <meta name="author" content="{{ $author }}" />
        <meta property="article:author" content="{{ $author }}" />
    @endif


<!-- META INFORMATION -->

    <!--[if lt IE 9]>
    <script src="{{ url('public/blog') }}/assets/js/html5shiv.min.js"></script>
    <script src="{{ url('public/blog') }}/assets/js/respond.min.js"></script>
    <![endif]-->

</head>