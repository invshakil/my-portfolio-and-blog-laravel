<div class="col-lg-2 col-md-2 col-sm-4 col-xs-12">
    <div class="row">
        <div class="left_bar">
            <div class="single_leftbar">
                <h2><span>Browse By Category</span></h2>
                <div class="singleleft_inner">
                    <ul class="label_nav">
                        @php $category = DB::table('categories')->get() @endphp
                        @foreach($category as $item)
                            <li><a href="{{ url('/blog/category/'.$item->category_id.'/'.$item->category_name) }}">{{ $item->category_name }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="single_leftbar wow fadeInDown">
                <h2><span>Side Ad</span></h2>
                <div class="single_leftbar wow fadeInDown">
                    <div class="singleleft_inner"><a href="#">
                            <img alt="" src="{{ url('public/blog') }}/images/262x218.jpg" class="img-responsive img-thumbnail"></a>
                    </div>
                </div>

                <div class="single_leftbar wow fadeInDown">
                    <div class="singleleft_inner"><a href="#">
                            <img alt="" src="{{ url('public/blog') }}/images/262x218.jpg" class="img-responsive img-thumbnail"></a>
                    </div>
                </div>

                <div class="single_leftbar wow fadeInDown">
                    <div class="singleleft_inner"><a href="#">
                            <img alt="" src="{{ url('public/blog') }}/images/262x218.jpg" class="img-responsive img-thumbnail"></a>
                    </div>
                </div>

                <div class="single_leftbar wow fadeInDown">
                    <div class="singleleft_inner"><a href="#">
                            <img alt="" src="{{ url('public/blog') }}/images/262x218.jpg" class="img-responsive img-thumbnail"></a>
                    </div>
                </div>

            </div>
            <div class="single_leftbar wow fadeInDown">
                <h2><span>Links</span></h2>
                <div class="singleleft_inner">
                    <ul class="link_nav">
                        <li><a href="{{ url('/') }}" target="_blank">My Portfolio</a></li>

                        <?php $fb = DB::table('banner_content')->where('info_type', 'facebook')->value('description'); ?>
                        <li><a href="{{$fb}}" target="_blank">My Facebook</a></li>

                        <li><a href="http://www.uvlsports.com" target="_blank">Universal Sports</a></li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>