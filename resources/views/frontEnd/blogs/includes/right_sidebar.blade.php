<div class="col-lg-4 col-md-4 col-xs-12">
    <div class="row">
        <div class="right_bar">
            <div class="single_leftbar">
                <h2><span>Search Content</span></h2>

                <div id="search-hide">
                    <form action="{{ url('blog/search-queries') }}" method="get">
                        <input type="text" size="40" name="search" placeholder="Search here ...">
                        <button class="remove"><span><i class="fa fa-search"></i></span></button>
                    </form>
                    {{--<button class="remove"><span><i class="fa fa-search"></i></span></button>--}}
                </div>

            </div>
            <div class="single_leftbar wow fadeInDown">
                <h2><span>Popular Post</span></h2>
                <div class="singleleft_inner">
                    <ul class="catg3_snav ppost_nav wow fadeInDown">
                        @php $popular = DB::table('articles')->orderBy('hit_count','desc')->where('status',1)->limit(10)->get();  @endphp
                        @foreach($popular as $item)
                            <li id="divEffect" class="shadow2 wow fadeInRight">
                                <div class="media">
                                    <a href="{{ url('/blog/article'.'/'.$item->article_id.'/'.str_replace(' ','-',$item->title) ) }}"
                                       class="media-left">
                                        <img src="{{ url($item->thumbnail) }}" class="img-responsive img-thumbnail"
                                             alt="{{ $item->title }}">
                                    </a>

                                    <div class="media-body">
                                        <a href="{{ url('/blog/article'.'/'.$item->article_id.'/'.str_replace(' ','-',$item->title) ) }}"
                                           class="catg_title">
                                            {{ $item->title }}
                                        </a>
                                    </div>
                                </div>
                            </li>
                        @endforeach


                    </ul>
                </div>
            </div>

            <div class="single_leftbar wow fadeInDown animated">
                <h2><span>Blog Archive</span></h2>
                <div class="singleleft_inner">
                    <div class="blog_archive">

                        @php
                            $archives = DB::table('articles')
                                    ->selectRaw('year(created_at) as year,month(created_at) as month, monthname(created_at) as monthname, COUNT(*) post_count')
                                    ->groupBy('year')
                                    ->groupBy('monthname')
                                    ->groupBy('month')
                                    ->orderBy('year', 'desc')
                                    ->orderBy('month', 'desc')
                                    ->where('status',1)
                                    ->get();

                        @endphp
                        <ul class="link_nav">
                            <li>
                                @foreach($archives as $archive)
                                    <a href="{{ route('archives', ['month' => $archive->monthname, 'year'=>$archive->year]) }}">
                                        {{ $archive->monthname }}, {{ $archive->year }} <b class="badge">{{ $archive->post_count }}</b>
                                    </a>
                                @endforeach
                            </li>
                        </ul>

                    </div>
                </div>
            </div>

            <div class="single_leftbar wow fadeInDown animated">
                <h2><span>Follow My Fan Page</span></h2>
                <div class="singleleft_inner">
                    <iframe src="https://www.facebook.com/plugins/likebox.php?href=https://www.facebook.com/sshakilofficial/&amp;width=285&amp;colorscheme=light&amp;show_faces=true&amp;border_color&amp;stream=false&amp;header=false&amp;height=400"
                            scrolling="yes" frameborder="0" allowtransparency="true"></iframe>
                </div>
            </div>

            <div class="single_leftbar wow fadeInDown">
                <h2><span>Links</span></h2>
                <div class="singleleft_inner">
                    <ul class="link_nav">
                        <li><a href="{{ url('/') }}" target="_blank">My Portfolio</a></li>

						<?php $fb = DB ::table('banner_content') -> where('info_type', 'facebook') -> value('description'); ?>
                        <li><a href="{{$fb}}" target="_blank">My Facebook</a></li>

                        <li><a href="http://www.uvlsports.com" target="_blank">Universal Sports</a></li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>