<footer id="footer">

    <div class="footer_top">
        @if (count($errors) > 0)
            <div>
                <ul>
                    @foreach ($errors->all() as $error)
                        <style>
                            .alert-danger{
                                background-color: red;
                                color: white;
                                font-weight: bold;
                                font-family: "Bookman Old Style";
                            }
                        </style>
                        <div class="alert alert-danger">
                            {{ $error }}
                        </div>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="col-lg-4 col-md-4">
            <div class="single_footer_top wow fadeInLeft">
                <h2>Follow By Email</h2>
                <div class="subscribe_area">
                    <p>"Subscribe here to get our newsletter, it is safe just Put your Email and click
                        subscribe"</p>

                    <form action="{{ route('subscribe') }}" method="post">
                        {{ csrf_field() }}
                        <div class="subscribe_mail">
                            <input class="form-control" type="email" name="email" placeholder="Email Address">
                            <i class="fa fa-envelope"></i></div>
                        <input class="submit_btn" type="submit" value="Submit">
                    </form>
                </div>
            </div>
            <div class="single_footer_top wow fadeInRight">
                <h2>Browse By Category</h2>
                <ul class="footer_labels">
                    @php $category = DB::table('categories')->get() @endphp
                    @foreach($category as $item)
                        <li><a href="{{ url('/blog/category/'.$item->category_id.'/'.$item->category_name) }}">{{ $item->category_name }}</a></li>
                    @endforeach
                </ul>
            </div>


        </div>
        <div class="col-lg-4 col-md-4">
            <div class="single_footer_top wow fadeInLeft">
                <h2>Popular Post</h2>
                <ul class="catg3_snav ppost_nav">
                    @php $popular = DB::table('articles')->orderBy('hit_count','desc')->where('status',1)->limit(5)->get();  @endphp
                    @foreach($popular as $item)
                        <li>
                            <div class="media">
                                <a href="{{ url('/blog/article'.'/'.$item->article_id.'/'.str_replace(' ','-',$item->title) ) }}" class="media-left">
                                    <img src="{{ url($item->thumbnail) }}" class="img-responsive img-thumbnail" alt="{{ $item->title }}">
                                </a>

                                <div class="media-body">
                                    <a href="{{ url('/blog/article'.'/'.$item->article_id.'/'.str_replace(' ','-',$item->title) ) }}" class="catg_title">
                                        {{ $item->title }}
                                    </a>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>

        <div class="col-lg-4 col-md-4">
            <div class="single_footer_top wow fadeInRight">
                <h2>Contact Form</h2>
                <form action="{{ route('mail.contact') }}" class="contact_form" method="post">
                    {{ csrf_field() }}
                    <label>Name*</label>
                    <input class="form-control" type="text" name="name" placeholder="Name">
                    <label>Email*</label>
                    <input class="form-control" type="email" name="email" placeholder="Email">
                    <label>Contact Number</label>
                    <input class="form-control" type="text" name="contact" placeholder="Optional">
                    <label>Message*</label>
                    <textarea class="form-control" name="message" cols="30" rows="10" placeholder="Your Message"></textarea>
                    <input class="send_btn" type="submit" value="Send">
                </form>
            </div>
        </div>
    </div>
    <div class="footer_bottom">
        <div class="footer_bottom_left">
            <p>Copyright &copy; 2017 BY <a href="http://www.sshakil.com">SHAKIL</a></p>
        </div>
        <div class="footer_bottom_right">
            <ul>
                <li><a class="tootlip" data-toggle="tooltip" data-placement="top" title="Twitter" href="#"><i
                                class="fa fa-twitter"></i></a></li>
                <li><a class="tootlip" data-toggle="tooltip" data-placement="top" title="Facebook" href="#"><i
                                class="fa fa-facebook"></i></a></li>
                <li><a class="tootlip" data-toggle="tooltip" data-placement="top" title="Googel+" href="#"><i
                                class="fa fa-google-plus"></i></a></li>
                <li><a class="tootlip" data-toggle="tooltip" data-placement="top" title="Youtube" href="#"><i
                                class="fa fa-youtube"></i></a></li>
                <li><a class="tootlip" data-toggle="tooltip" data-placement="top" title="Rss" href="#"><i
                                class="fa fa-rss"></i></a></li>
            </ul>
        </div>
    </div>
</footer>