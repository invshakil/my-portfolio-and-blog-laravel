<header id="header">
    <div class="header_top">
        <nav class="navbar navbar-default navbar-fixed-top navbar-fixed-width"  role="navigation">
            <div class="container-fluid" style="padding-right: 0!important;">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#navbar" aria-expanded="false" aria-controls="navbar"><span
                                class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span
                                class="icon-bar"></span> <span class="icon-bar"></span></button>
                </div>
                <div id="navbar" class="navbar-collapse collapse" style="padding-right: 0!important;">

                    <ul class="nav navbar-nav custom_nav">
                        <li @if(request()->url() == route('blog')) class="active" @endif><a href="{{ url('/blog') }}">Home</a>
                        </li>

                        @php $category = DB::table('categories')->get() @endphp
                        @foreach($category as $item)
                            <li @if(request()->url() == route('blog.category',['id'=>$item->category_id,'title'=> $item->category_name])) class="active" @endif>
                                <a href="{{ url('/blog/category/'.$item->category_id.'/'.$item->category_name) }}">{{ $item->category_name }}</a>
                            </li>
                        @endforeach

                        <li><a href="{{ url('/') }}" target="_blank">My Portfolio</a></li>
                        {{--<li><a href="#">Contact</a></li>--}}
                        @php $id = Session::get('id') @endphp

                    </ul>

                    <ul class="nav navbar-nav custom_nav navbar-right" >

                        @if(Auth::check())
                            <li class="dropdown">
                                <a href="javascript:void(0)" class="dropbtn"><i class="fa fa-user-circle-o"></i> Account</a>
                                <div class="dropdown-content">
                                    <a href="{{ url('/admin_dashboard') }}"><i class="fa fa-code"></i> Dashboard</a>
                                    <a href="{{ url('/register') }}"><i class="fa fa-sign-out"></i> Logout</a>
                                </div>
                            </li>
                        @else

                            <li class="dropdown">
                                <a href="javascript:void(0)" class="dropbtn"><i class="fa fa-user-circle-o"></i> Account</a>
                                <div class="dropdown-content">
                                    <a href="{{ url('/login') }}"><i class="fa fa-sign-in"></i> Sign In</a>
                                    <a href="{{ url('/register') }}"><i class="fa fa-user-plus"></i> Sign Up</a>
                                </div>
                            </li>
                        @endif
                    </ul>

                </div>
            </div>
            {{--<div class="header_search">--}}

                {{----}}

            {{--</div>--}}
        </nav>

    </div>
    <div class="header_bottom">
        <div class="logo_area"><a class="logo" href="{{ url('/blog') }}"><b>Shakil's</b> Blog
                <span>Software Developer</span></a>
        </div>

    </div>
</header>

<div class="latest_newsarea"><span>Latest Article</span>
    <ul id="ticker01" class="news_sticker">
        @php $title = DB::table('articles')->latest()->where('status',1)->limit(5)->get();  @endphp
        @foreach($title as $item)
            <li>
                <a href="{{ url('/blog/article'.'/'.$item->article_id.'/'.str_replace(' ','-',$item->title) ) }}">{{ $item->title }}</a>
            </li>
        @endforeach
    </ul>
</div>