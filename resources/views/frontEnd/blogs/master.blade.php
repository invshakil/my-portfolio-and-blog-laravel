<!DOCTYPE html>
<html>
@include('frontEnd.blogs.includes.header_files')

<style>
    #divEffect{
        border-radius:5px!important;
        background-color:#ffffff;!important;
        padding:10px 10px;
        /*box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 20px rgba(0, 0, 0, 0.2) inset;*/
        margin-bottom: 10px;
        -moz-box-shadow: 1px 1px 3px 2px #DCDCDC;
        -webkit-box-shadow: 1px 1px 3px 2px #DCDCDC;
        box-shadow:         1px 1px 3px 2px #DCDCDC;"

    }

    .navbar-right{
        margin-right: 1px;
    }

    .carousel-caption {
        position: absolute;
        right: 0;
        bottom: 0;
        left: 0!important;
        padding: 7px;
        background: rgba(0, 0, 0, 0.6)!important;
        font-family: "Bookman Old Style";
        font-weight: 600;

        border-radius:5px!important;

    }
    .carousel-caption p {
        margin-bottom: 0;
    }

    .img-rounded{
        -webkit-filter: blur(2px);
        -moz-filter: blur(2px);
        -o-filter: blur(2px);
        -ms-filter: blur(2px);
        filter: blur(2px);
    }

    pre{
        -moz-box-shadow: 1px 1px 3px 2px #555;
        -webkit-box-shadow: 1px 1px 3px 2px #555;
        box-shadow:         1px 1px 3px 2px #555;"
    }

    @media screen and (max-width: 700px){
        .carousel-caption p {
            font-size: 13px;
        }
        .carousel-caption {
            background: rgba(0, 0, 0, 0.55);
        }
        .carousel-control {
            top: 20%;
        }

        .dropdown:hover .dropdown-content{
            left: 0!important;
        }

    }

    @media all and (min-width: 768px) {
        .navbar-fixed-width {
            width: 768px;
            margin-left: auto;
            margin-right:auto;
            border-bottom: 3px solid #47c9af!important;
        }
    }

    @media all and (min-width: 992px) {
        .navbar-fixed-width {
            width: 992px;
            margin-left: auto;
            margin-right:auto;
            border-bottom: 3px solid #47c9af!important;
        }

    }

    @media all and (min-width: 1200px) {
        .navbar-fixed-width {
            width: 1200px;
            margin-left: auto;
            margin-right:auto;
            border-bottom: 3px solid #47c9af!important;
        }
    }

</style>
<body>

<div id="preloader">
    <div id="status">&nbsp;</div>
</div>

<a class="scrollToTop" href="#"><i class="fa fa-angle-up"></i></a>

<div class="container">
    <div class="box_wrapper">
        @include('frontEnd.blogs.includes.top_header')

        <section id="contentbody">
            <div class="row">

                @yield('main_content')

                {{--@include('frontEnd.blogs.includes.left_sidebar')--}}

                @include('frontEnd.blogs.includes.right_sidebar')
            </div>
        </section>
        @include('frontEnd.blogs.includes.footer')
    </div>
</div>

@include('frontEnd.blogs.includes.footer_script')



</body>
</html>