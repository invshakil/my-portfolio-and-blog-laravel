@extends('frontEnd.blogs.master')

@section('main_content')
    <div class="col-lg-8 col-md-8 col-xs-12">

        <div class="row">
            <div class="middle_bar">
                <div class="featured_sliderarea fadeIn animated">
                    <div id="myCarousel" class="carousel slide" style="max-width: 900px; margin: 0 auto;">
                        <ol class="carousel-indicators hidden-xs hidden-sm" style="padding-bottom: 42px;">
                            @php $slider = DB::table('articles')->latest()->where('slider_status',1)->where('status',1)->limit(3)->get();  @endphp

                            @foreach($slider as $item)
                                <li data-target="#myCarousel" data-slide-to="{{ $loop->index }}"
                                    class="{{ $loop->first ? ' active' : '' }}"></li>
                            @endforeach

                        </ol>
                        <div class="carousel-inner">

                            @foreach($slider as $item)
                                <div class="item {{ $loop->first ? ' active' : '' }}">
                                    <a href="{{ url('/blog/article'.'/'.$item->article_id.'/'.str_replace(' ','-',$item->title) ) }}">
                                        <img src="{{ url($item->image) }}" alt="{{ $item->title }}" class="img-rounded">
                                    </a>
                                    <div class="carousel-caption">
                                        <h3>
                                            <a href="{{ url('/blog/article'.'/'.$item->article_id.'/'.str_replace(' ','-',$item->title) ) }}"> {{ $item->title }}</a>
                                        </h3>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                        <a class="left left_slide" href="#myCarousel" role="button" data-slide="prev"> <span
                                    class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> </a>
                        <a class="right right_slide" href="#myCarousel" role="button" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> </a>
                    </div>

                </div>


                <div class="single_category wow fadeInLeftBig animated" >
                    <div class="category_title"><a href="#">Latest Articles</a></div>
                    <div class="single_category_inner">
                        <ul class="catg_nav" >
                            @php $content = DB::table('articles as a')
                            ->join('categories as c', 'a.category', '=', 'c.category_id')
                            ->join('users as u', 'a.author', '=', 'u.id')
                            ->latest()
                            ->where('a.status',1)
                            ->select('a.*', 'u.name', 'c.category_name')
                            ->paginate(10);
                            @endphp

                            @foreach($content as $item)
                                <li id="divEffect" class="shadow2 wow fadeInRight">
                                    <div class="catgimg_container">
                                        <a class="catg1_img"
                                           href="{{ url('/blog/article'.'/'.$item->article_id.'/'.str_replace(' ','-',$item->title) ) }}">
                                            <img src="{{ url($item->m_size_img) }}" class="img-responsive img-thumbnail"
                                                 alt="{{ $item->title }}">
                                        </a>
                                    </div>
                                    <a class="catg_title"
                                       href="{{ url('/blog/article'.'/'.$item->article_id.'/'.str_replace(' ','-',$item->title) ) }}">
                                        {{ $item->title }}
                                    </a>
                                    <div class="sing_commentbox">
                                        <p>
                                            <i class="fa fa-calendar"></i>{{ date('d-M h:i A', strtotime($item->created_at)) }}
                                        </p>&nbsp;&nbsp;
                                        <a href="#"><i class="fa fa-user"></i>{{ $item->name }}</a>&nbsp;&nbsp;&nbsp;
                                        <a href="#"><i class="fa fa-tags"></i>

                                            @php $categories = explode(',',$item->category); @endphp
                                            @foreach($categories as $category)

                                                @php $cat_name = DB::table('categories')->where('category_id',$category)->value('category_name') @endphp

                                                @php
                                                    if (next($categories) == TRUE)
                                                {
                                                    echo $cat_name.', ';
                                                }
                                                else
                                                {
                                                    echo $cat_name;
                                                }
                                                @endphp


                                            @endforeach
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="single_category_inner">
                        {{ $content->links() }}
                    </div>

                </div>

            </div>
        </div>
    </div>


@endsection