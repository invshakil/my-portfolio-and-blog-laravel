@extends('frontEnd.blogs.master')

@section('main_content')
    <div class="col-lg-8 col-md-8 col-xs-12">

        <div class="row">
            <div class="middle_bar">

                <div class="single_category wow fadeInDown">
                    <div class="category_title"><a href="#">Search Result of Keyword "{{ $search }}"</a></div>
                    <div class="single_category_inner">
                        <ul class="catg_nav">

                            @if(isset($details) && count($details) > 0)

                                @foreach($details as $item)
                                    <li id="divEffect" class="shadow2 wow fadeInRight">
                                        <div class="catgimg_container">
                                            <a class="catg1_img"
                                               href="{{ url('/blog/article'.'/'.$item->article_id.'/'.str_replace(' ','-',$item->title) ) }}">
                                                <img src="{{ url($item->m_size_img) }}"
                                                     class="img-responsive img-thumbnail"
                                                     alt="{{ $item->title }}">
                                            </a>
                                        </div>
                                        <a class="catg_title"
                                           href="{{ url('/blog/article'.'/'.$item->article_id.'/'.str_replace(' ','-',$item->title) ) }}">
                                            {{ $item->title }}
                                        </a>
                                        <div class="sing_commentbox">
                                            <p>
                                                <i class="fa fa-calendar"></i>{{ date('d M y h:i A', strtotime($item->created_at)) }}
                                            </p>&nbsp;&nbsp;
                                            <a href="#"><i class="fa fa-user"></i>{{ $item->name }}</a>&nbsp;&nbsp;&nbsp;
                                            <a href="#"><i class="fa fa-tags"></i>{{ $item->category_name }}</a>
                                        </div>
                                    </li>
                                @endforeach
                            @else
                                <h3 align="center">NO RECORD FOUND!</h3>
                            @endif
                        </ul>
                    </div>
                    <div class="single_category_inner">
                        {{ $details->links() }}
                    </div>

                </div>

            </div>
        </div>
    </div>

@endsection