@extends('frontEnd.blogs.master')

@section('main_content')
	<?php
	$id = $details -> article_id;
	$value = Session ::get('hit_count' . $id);

	if ($value == NULL) {
		$hit_count = $details -> hit_count + 1;

		$data = array ('hit_count' => $hit_count);

		DB ::table('articles') -> where('article_id', $id) -> update($data);

		Session ::put('hit_count' . $id, 1);
	}

	?>


    <div class="col-lg-8 col-md-8 col-xs-12">
        <div class="row">
            <div class="middle_bar" >
                <div class="single_post_area">
                    <ol class="breadcrumb shadow2 wow fadeInRight"  id="divEffect" >
                        <li><a href="{{ url('/') }}"><i class="fa fa-home"></i>Home<i class="fa fa-angle-right"></i></a>
                        </li>
                        <li>
                            <a href="{{ url('/blog/category/'.$details->category.'/'.$details->category_name) }}">{{ $details->category_name }}
                                <i class="fa fa-angle-right"></i></a></li>
                        <li class="active">{{$details->title}}</li>
                    </ol>
                    <h2 class="post_title wow "> {{$details->title}} </h2>
                    <a href="#" class="author_name"><i class="fa fa-user"></i>{{ $details->name }}</a>
                    <a href="#" class="author_name"><i class="fa fa-tags"></i>
                        @php $categories = explode(',',$details->category); @endphp
                        @foreach($categories as $category)

                            @php $cat_name = DB::table('categories')->where('category_id',$category)->value('category_name') @endphp

                            @php
                                if (next($categories) == TRUE)
                            {
                                echo $cat_name.', ';
                            }
                            else
                            {
                                echo $cat_name;
                            }
                            @endphp


                        @endforeach
                    </a>
                    <a href="#"
                       class="fa fa-clock-o"></i> {{ date('d F, Y : h:i A', strtotime($details->created_at)) }}</a>
                    <br/><br/>
                    <img src="{{ url($details->image) }}" class="img-responsive img-thumbnail" width="100%"
                         alt="{{ $details->title }}">

                    <div class="single_post_content" class="wow fadeInRight">
                        {!! htmlspecialchars_decode($details->description) !!}

                        <hr/>

                        @php $tags = explode(',',$details->tags) @endphp
                        <div class="post_title wow" style="font-size: 14px;">
                            <i class="fa fa-tags"> </i> TAGS:
                            @foreach($tags as $tag)
                                <a class="btn btn-info" href="{{ route('article.tag', ['name'=>str_replace(' ','-',$tag)]) }}">{{ $tag }}</a>
                            @endforeach
                        </div>


                        <div class="post_title wow" style="font-size: 14px;">
                            <i class="fa fa-eye"> </i> Views: {{ $details->hit_count }}
                            <br>
                            <div class="fb-like"
                                 data-href="{{ url('/blog/article'.'/'.$details->article_id.'/'.str_replace(' ','-',$details->title) ) }}"
                                 data-width="320" data-layout="standard" data-action="like" data-size="small"
                                 data-show-faces="false">
                            </div>
                        </div>


                        <!-- DISQUS COMMENT SECTION -->

                        <div id="disqus_thread"></div>

                        <input type="hidden" id="url" value="{{ url('/blog/article'.'/'.$details->article_id.'/'.str_replace(' ','-',$details->title) ) }}">
                        <input type="hidden" id="id" value="<?php echo $details->article_id; ?>">
                        <script>

                             var disqus_config = function () {
                             this.page.url = $('#url').val();
                             this.page.identifier = $('#id').val();
                             };

                            (function () { // DON'T EDIT BELOW THIS LINE
                                var d = document, s = d.createElement('script');
                                s.src = 'https://https-www-sshakil-com.disqus.com/embed.js';
                                s.setAttribute('data-timestamp', +new Date());
                                (d.head || d.body).appendChild(s);
                            })();
                        </script>

                        <!-- DISQUS COMMENT SECTION -->
                    </div>

                    <div class="social_area wow fadeInLeft">
                        <ul>
                            <li><a target="_blank"
                                   href="http://www.facebook.com/sharer.php?u={{url()->current()}}"><span
                                            class="fa fa-facebook"></span></a></li>
                            <li><a target="_blank" href="http://twitter.com/home?status={{url()->current()}}"><span
                                            class="fa fa-twitter"></span></a></li>
                            <li><a target="_blank"
                                   href="http://www.linkedin.com/shareArticle?mini=true&url={{url()->current()}}"><span
                                            class="fa fa-linkedin"></span></a></li>
                            <li><a target="_blank" href="https://plus.google.com/share?url={{url()->current()}}"><span
                                            class="fa fa-google-plus"></span></a></li>
                            <li><a target="_blank" href="#"><span class="fa fa-pinterest"></span></a></li>
                        </ul>
                    </div>
                    <div class="related_post">
                        <h2 class="wow fadeInLeftBig">Related Posts you may like <i class="fa fa-thumbs-o-up"></i></h2>
                        <ul class="recentpost_nav relatedpost_nav wow fadeInDown animated">
                            @php $similar = DB::table('articles')->inRandomOrder()->where('status',1)->where('category',$details->category)->limit(3)->get();  @endphp
                            @foreach($similar as $item)
                                <li>
                                    <a href="{{ url('/blog/article'.'/'.$item->article_id.'/'.str_replace(' ','-',$item->title) ) }}">
                                        <img src="{{ url($item->m_size_img) }}" class="img-responsive img-thumbnail"
                                             alt="{{ $item->title }}">
                                    </a>
                                    <a href="{{ url('/blog/article'.'/'.$item->article_id.'/'.str_replace(' ','-',$item->title) ) }}"> {{ $item->title }}</a>
                                </li>
                            @endforeach


                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <style>
        blockquote {
            font-size: 13px !important;
        }
    </style>

    <script
            src="https://code.jquery.com/jquery-2.2.4.js"
            integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/styles/atom-one-dark.min.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>

    <script>hljs.initHighlightingOnLoad();</script>
    <script>
        $(document).ready(function () {
            $('img').addClass('img-responsive img-thumbnail img-center').css({
                width: 'auto',
                height: 'auto'
            });

        });


        hljs.configure({useBR: true});
        $(document).ready(function () {
            $('div blockquote').each(function (i, block) {
                hljs.highlightBlock(block);
            });
        });

        $(document).ready(function () {
            $('div pre').each(function (i, block) {
                hljs.highlightBlock(block);
            });
        });

    </script>


    <div id="fb-root"></div>

    <script>(function (d, s, id) {

            var js, fjs = d.getElementsByTagName(s)[0];

            if (d.getElementById(id))
                return;

            js = d.createElement(s);

            js.id = id;

            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";

            fjs.parentNode.insertBefore(js, fjs);

        }(document, 'script', 'facebook-jssdk'));
    </script>
    <script id="dsq-count-scr" src="//https-www-sshakil-com.disqus.com/count.js" async></script>
@endsection