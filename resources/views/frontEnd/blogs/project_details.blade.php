@extends('frontEnd.blogs.master')

@section('main_content')

    <div class="col-lg-8 col-md-8 col-xs-12">
        <div class="row">
            <div class="middle_bar">
                <div class="single_post_area">
                    <ol class="breadcrumb">
                        <li><a href="{{ url('/') }}"><i class="fa fa-home"></i>Home<i class="fa fa-angle-right"></i></a>
                        </li>
                        <li>
                            <a href="#">Projects
                                <i class="fa fa-angle-right"></i></a></li>
                        <li class="active">{{ $details->title }}</li>
                    </ol>
                    <h2 class="post_title wow "> {{ $details->title }} </h2>

                    <img src="{{ url($details->featured_image) }}" class="img-responsive img-thumbnail" width="100%"
                         alt="">

                    <div class="single_post_content">
                        {!! $details->description !!}

                        <hr/>

                        <div class="post_title wow" style="font-size: 16px;">
                            <i class="fa fa-link"> </i> Demo Link: <a href="{{ $details->demo_link }}"
                                           target="_blank">{{ $details->demo_link }}</a>
                        </div>
                        @php $tags = explode(',',$details->tags) @endphp
                        <div class="post_title wow" style="font-size: 16px;">
                            <i class="fa fa-tags"> </i> TAGS:
                            @foreach($tags as $tag)
                                <button class="btn btn-info">{{ $tag }}</button>
                            @endforeach
                        </div>
                    </div>

                    <div class="social_area wow fadeInLeft">
                        <ul>
                            <li><a href="http://www.facebook.com/sharer.php?u={{url()->current()}}"><span
                                            class="fa fa-facebook"></span></a></li>
                            <li><a href="http://twitter.com/home?status={{url()->current()}}"><span
                                            class="fa fa-twitter"></span></a></li>
                            <li><a href="http://www.linkedin.com/shareArticle?mini=true&url={{url()->current()}}"><span
                                            class="fa fa-linkedin"></span></a></li>
                            <li><a href="https://plus.google.com/share?url={{url()->current()}}"><span
                                            class="fa fa-google-plus"></span></a></li>
                            <li><a href="#"><span class="fa fa-pinterest"></span></a></li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection