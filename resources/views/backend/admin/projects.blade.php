@extends('backend.master')

@section('title')
    Project Publish
@endsection

@section('main_content')
    <script src="{{url('public')}}/assets/js/bootstrap-tagsinput.min.js"></script>

    <div class="panel minimal minimal-gray">

        @if (count($errors) > 0)
            <div>
                <ul>
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">
                            {{ $error }}
                        </div>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="panel-heading">
            <div class="panel-title"><h2>Project Add/Manage</h2></div>
            <div class="panel-options">

                <ul class="nav nav-tabs">
                    <li class="active"><a href="#profile-1" data-toggle="tab">Project List</a></li>
                    <li><a href="#profile-2" data-toggle="tab">Add Project</a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">

            <div class="tab-content">
                <div class="tab-pane active" id="profile-1">
                    <div class="panel panel-dark" data-collapsed="0">
                        <!-- panel head -->
                        <div class="panel-heading">
                            <div class="panel-title">Project List</div>

                            <div class="panel-options">
                                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1"
                                   class="bg"><i
                                            class="entypo-cog"></i></a>
                                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                            </div>
                        </div>

                        <!-- panel body -->
                        <div class="panel-body table-responsive">

                            <table class="table table-bordered datatable" id="table-1">
                                <thead>
                                <tr>
                                    <th width="10%">ID</th>
                                    <th width="20%">Image</th>
                                    <th width="20%">Title</th>
                                    <th width="20%">Tags</th>
                                    <th width="20%">Demo Link</th>
                                    <th width="10%">Status</th>
                                    <th width="10%">Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $result = DB::table('projects')
                                    ->get();?>

                                @foreach($result->all() as $row)
                                    <tr>

                                        <td class="center">{{ $row->project_id }}</td>
                                        <td class="center"><img src="{{ url($row->featured_image) }}" width="50"></td>
                                        <td class="center">{{ $row->title }}</td>

                                        <td class="center">
                                            @php $tags = explode(',',$row->tags) @endphp
                                            @foreach($tags as $tag)
                                                <button class="btn btn-danger">{{ $tag }}</button>
                                            @endforeach
                                        </td>

                                        <td class="center">{{ $row->demo_link }}</td>

                                        <td class="center">
                                            @if($row->status==1)
                                                <div class="label label-success">Published</div>
                                            @else
                                                <div class="label label-danger">Pending</div>
                                            @endif
                                        </td>
                                        <td class="center">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default btn-sm dropdown-toggle"
                                                        data-toggle="dropdown">
                                                    Action <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                    <!-- EDITING LINK -->
                                                    <li>
                                                        <a href="#"
                                                           data-id="{{ $row->project_id }}"

                                                           data-toggle="modal"
                                                           data-target="#updateModal"
                                                        >
                                                            <i class="fa fa-pencil-square-o"></i>
                                                            edit
                                                        </a>
                                                    </li>
                                                    <li class="divider"></li>

                                                    <!-- DELETION LINK -->
                                                    <li>
                                                        <a href="#" data-toggle="modal" data-id="{{$row->project_id}}"
                                                           data-target="#confirm-delete">
                                                            <i class="fa fa-trash-o"></i>
                                                            delete
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>


                                @endforeach

                                </tbody>

                            </table>
                        </div>
                    </div>


                </div>

                <div class="tab-pane" id="profile-2">
                    <div class="panel panel-dark" data-collapsed="0">
                        <!-- panel head -->
                        <div class="panel-heading">
                            <div class="panel-title">Add Project</div>

                            <div class="panel-options">
                                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1"
                                   class="bg"><i
                                            class="entypo-cog"></i></a>
                                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                            </div>
                        </div>

                        <!-- panel body -->
                        <div class="panel-body">

                            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                                  action="{{ URL::to('/admin_dashboard/projects/create') }}"
                                  enctype="multipart/form-data">

                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Title</label>

                                    <div class="col-sm-5">
                                        <input type="text" name="title"
                                               class="form-control" id="field-1"
                                               placeholder="Enter Title">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Project Featured Image</label>

                                    <div class="col-sm-5">

                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="width: 200px; "
                                                 data-trigger="fileinput">
                                                <img src="http://placehold.it/200x150">
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail"
                                                 style="max-width: 200px; max-height: 150px"></div>
                                            <div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="featured_image" accept="image/*">
											</span>
                                                <a href="#" class="btn btn-orange fileinput-exists"
                                                   data-dismiss="fileinput">Remove</a>
                                            </div>
                                        </div>

                                    </div>
                                    <span style="background-color: #0a001f;color: #fff;font-weight: bold">Note: Image size maximum 300kb.</span>
                                </div>

                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Description</label>

                                    <div class="col-sm-8">
                                        <textarea name="description" class="form-control ckeditor"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Enter Tags</label>

                                    <div class="col-sm-5">
                                        <input type="text" name="tags" placeholder="Tags"
                                               class="tm-input form-control tm-input-info"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Demo Link</label>

                                    <div class="col-sm-5">
                                        <input type="text" name="demo_link" placeholder="Enter Demo Link"
                                               class=" form-control "/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Visibility Status</label>

                                    <div class="col-sm-5">
                                        <select name="status" class="form-control">
                                            <option value="1">Published</option>
                                            <option value="0">Pending</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" id="submit" class="btn btn-primary btn-icon">Add Project Information <i class="entypo-check"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>


    </div>

    <script type="text/javascript">
        $('form input').on('keypress', function (e) {
            return e.which !== 13;
        });

        var responsiveHelper;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        var tableContainer;

        jQuery(document).ready(function ($) {
            tableContainer = $("#table-1");

            tableContainer.dataTable({
                "sPaginationType": "bootstrap",
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bStateSave": true,


                // Responsive Settings
                bAutoWidth: false,
                fnPreDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper) {
                        responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                    }
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    responsiveHelper.createExpandIcon(nRow);
                },
                fnDrawCallback: function (oSettings) {
                    responsiveHelper.respond();
                }
            });

            $(".dataTables_wrapper select").select2({
                minimumResultsForSearch: -1
            });
        });

    </script>

    @include('backend.admin.modal.modal_edit_projects')

    <script type="text/javascript">
        $('#updateModal').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id'); //<- It's data-id value
            $('#id').val(id);

            $.ajax({
                url: "{{ url('/admin_dashboard/projects/') }}"+'/'+id,
                method: "POST",
                data: {article_id: id},
                dataType: "json",
                success: function (data) {

                    $('#title').val(data.title);
                    $('textarea#tags').val(data.tags);
                    $('#demo_link').val(data.demo_link);

                    var markupStr = data.description;

                    $('#description').summernote('code', markupStr);



                    $('select#status').val(data.status);

                    $('img#featured_image').attr('src', '{{ URL::to('/') }}/' + data.featured_image);

                }
            });

        });

        $('#confirm-delete').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id'); //<- It's data-id value

            $('a#id').attr('href', '{{URL::to('/admin_dashboard/projects/delete')}}/' + id);
        });
    </script>

@endsection