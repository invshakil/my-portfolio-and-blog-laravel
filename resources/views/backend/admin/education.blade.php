@extends('backend.master')

@section('title')
    Education Management
@endsection

@section('main_content')
    <script src="{{url('public')}}/assets/js/bootstrap-tagsinput.min.js"></script>

    <div class="panel minimal minimal-gray">

        @if (count($errors) > 0)
            <div>
                <ul>
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">
                            {{ $error }}
                        </div>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="panel-heading">
            <div class="panel-title"><h2>Education Add/Manage</h2></div>
            <div class="panel-options">

                <ul class="nav nav-tabs">
                    <li class="active"><a href="#profile-1" data-toggle="tab">Education List</a></li>
                    <li><a href="#profile-2" data-toggle="tab">Add Education</a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">

            <div class="tab-content">
                <div class="tab-pane active" id="profile-1">
                    <div class="panel panel-dark" data-collapsed="0">
                        <!-- panel head -->
                        <div class="panel-heading">
                            <div class="panel-title">Education List</div>

                            <div class="panel-options">
                                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1"
                                   class="bg"><i
                                            class="entypo-cog"></i></a>
                                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                            </div>
                        </div>

                        <!-- panel body -->
                        <div class="panel-body table-responsive">

                            <table class="table table-bordered datatable" id="table-1">
                                <thead>
                                <tr>
                                    <th width="10%">ID</th>
                                    <th width="20%">Session</th>
                                    <th width="20%">Institute</th>
                                    <th width="20%">Degree</th>
                                    <th width="20%">Subject</th>
                                    <th width="10%">Result</th>
                                    <th width="10%">Status</th>
                                    <th width="10%">Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $result = DB::table('education')
                                    ->get();?>

                                @foreach($result->all() as $row)
                                    <tr>

                                        <td class="center">{{ $row->education_id }}</td>
                                        <td class="center">{{ $row->session }}</td>
                                        <td class="center">
                                            <div class="label label-info">{{ $row->institute_name }}</div>
                                        </td>
                                        <td class="center">{{ $row->degree_name }}</td>
                                        <td class="center">{{ $row->subject }}</td>
                                        <td class="center">{{ $row->result }}</td>

                                        <td class="center">
                                            @if($row->status==1)
                                                <div class="label label-success">Published</div>
                                            @else
                                                <div class="label label-danger">Pending</div>
                                            @endif
                                        </td>
                                        <td class="center">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default btn-sm dropdown-toggle"
                                                        data-toggle="dropdown">
                                                    Action <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                    <!-- EDITING LINK -->
                                                    <li>
                                                        <a href="#"
                                                           data-id="{{ $row->education_id }}"
                                                           data-session="{{ $row->session }}"
                                                           data-institute_name="{{ $row->institute_name }}"
                                                           data-degree_name="{{ $row->degree_name }}"
                                                           data-subject="{{ $row->subject }}"
                                                           data-result="{{ $row->result }}"
                                                           data-status="{{ $row->status }}"
                                                           data-toggle="modal"
                                                           data-target="#updateModal"
                                                        >
                                                            <i class="fa fa-pencil-square-o"></i>
                                                            edit
                                                        </a>
                                                    </li>
                                                    <li class="divider"></li>

                                                    <!-- DELETION LINK -->
                                                    <li>
                                                        <a href="#" data-toggle="modal" data-id="{{$row->education_id}}"
                                                           data-target="#confirm-delete">
                                                            <i class="fa fa-trash-o"></i>
                                                            delete
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>


                                @endforeach

                                </tbody>

                            </table>
                        </div>
                    </div>


                </div>

                <div class="tab-pane" id="profile-2">
                    <div class="panel panel-dark" data-collapsed="0">
                        <!-- panel head -->
                        <div class="panel-heading">
                            <div class="panel-title">Add Education</div>

                            <div class="panel-options">
                                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1"
                                   class="bg"><i
                                            class="entypo-cog"></i></a>
                                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                            </div>
                        </div>

                        <!-- panel body -->
                        <div class="panel-body">

                            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                                  action="{{ URL::to('/admin_dashboard/education/create') }}"
                                  enctype="multipart/form-data">

                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Session</label>

                                    <div class="col-sm-5">
                                        <input type="text" name="session"
                                               class="form-control" id="field-1"
                                               placeholder="Enter Session">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Institute Name</label>

                                    <div class="col-sm-8">
                                        <textarea name="institute_name" rows="3" class="form-control"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Degree Name</label>

                                    <div class="col-sm-5">
                                        <input type="text" name="degree_name" placeholder="Degree Name"
                                               class="form-control"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Subject</label>

                                    <div class="col-sm-5">
                                        <input type="text" name="subject" placeholder="Subject"
                                               class="form-control"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Result</label>

                                    <div class="col-sm-5">
                                        <input type="text" name="result" placeholder="Result"
                                               class="form-control"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Publication Status</label>

                                    <div class="col-sm-5">
                                        <select name="status" class="form-control">
                                            <option value="1">Published</option>
                                            <option value="0">Pending</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" id="submit" class="btn btn-success btn-icon">Add Education Info <i class="entypo-check"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>


    </div>

    <script type="text/javascript">
        $('form input').on('keypress', function (e) {
            return e.which !== 13;
        });

        var responsiveHelper;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        var tableContainer;

        jQuery(document).ready(function ($) {
            tableContainer = $("#table-1");

            tableContainer.dataTable({
                "sPaginationType": "bootstrap",
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bStateSave": true,


                // Responsive Settings
                bAutoWidth: false,
                fnPreDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper) {
                        responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                    }
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    responsiveHelper.createExpandIcon(nRow);
                },
                fnDrawCallback: function (oSettings) {
                    responsiveHelper.respond();
                }
            });

            $(".dataTables_wrapper select").select2({
                minimumResultsForSearch: -1
            });
        });

    </script>

    @include('backend.admin.modal.modal_edit_education')

    <script type="text/javascript">
        $('#updateModal').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id'); //<- It's data-id value
            var session = $(e.relatedTarget).data('session'); //<- It's data-name value
            var institute_name = $(e.relatedTarget).data('institute_name'); //<- It's data-description value
            var degree_name = $(e.relatedTarget).data('degree_name'); //<- It's data-description value
            var subject = $(e.relatedTarget).data('subject'); //<- It's data-status value
            var result = $(e.relatedTarget).data('result'); //<- It's data-status value
            var status = $(e.relatedTarget).data('status'); //<- It's data-status value

            $('#id').val(id);
            $('#session').val(session);
            $('textarea#institute_name').val(institute_name);

            $('#degree_name').val(degree_name);
            $('#subject').val(subject);
            $('#result').val(result);

            $('select#status').val(status);

        });

        $('#confirm-delete').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id'); //<- It's data-id value

            $('a#id').attr('href', '{{URL::to('/admin_dashboard/education/delete')}}/' + id);
        });
    </script>



@endsection