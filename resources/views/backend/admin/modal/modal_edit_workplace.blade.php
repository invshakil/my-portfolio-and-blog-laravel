<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">&times;
                    </button>
                    <h4 class="modal-title">Edit
                        Article Information</h4>
                </div>
                <div class="modal-body">

                    <form role="form" class="form-horizontal form-groups-bordered" method="post"
                          action="{{ URL::to('/admin_dashboard/workplace/do_update/') }}"
                          enctype="multipart/form-data">

                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Duration</label>

                            <div class="col-sm-5">
                                <input type="text" name="duration"
                                       class="form-control" id="duration"
                                       placeholder="Enter Duration">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Company Name</label>

                            <div class="col-sm-8">
                                <textarea name="company_name" id="company_name" rows="3" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Designation</label>

                            <div class="col-sm-5">
                                <input type="text" name="designation" id="designation" placeholder="Designation"
                                       class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Language</label>

                            <div class="col-sm-5">
                                <input type="text" name="language" id="language" placeholder="Language"
                                       class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Description</label>

                            <div class="col-sm-9">
                                        <textarea type="text" name="description" id="description" rows="5"
                                                  class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Publication Status</label>

                            <div class="col-sm-5">
                                <select name="status" id="status" class="form-control">
                                    <option value="1">Published</option>
                                    <option value="0">Pending</option>
                                </select>
                            </div>
                        </div>

                        <input type="hidden" name="workplace_id"
                               class="form-control" id="id"
                               placeholder="Enter Title">

                        <button type="submit" class="btn btn-success btn-icon col-md-offset-3">
                            <i class="entypo-check"></i>
                            Apply Changes
                        </button>

                    </form>


                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">
                        Close
                    </button>
                </div>
            </div>

        </div>
    </div>


    <!-- (Normal Modal)-->
    <div class="modal fade" id="confirm-delete">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top:100px;">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align:center;">Are you sure to delete this information ?</h4>
                </div>


                <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                    <a href="" id="id"
                       class="btn btn-danger btn-ok">Delete</a>
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

</div>