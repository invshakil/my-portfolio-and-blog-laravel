<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">&times;
                    </button>
                    <h4 class="modal-title">Edit
                        Category Information</h4>
                </div>
                <div class="modal-body">

                    <form role="form"
                          class="form-horizontal form-groups-bordered"
                          method="post"
                          action="{{ URL::to('/admin_dashboard/categories/do_update/') }}"
                          enctype="multipart/form-data">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="field-1">Category
                                Name</label>

                            <div class="">
                                <input type="text" name="category_name"
                                       id="modal_name" value=""
                                       class="form-control"
                                       placeholder="Enter Category Name">
                                <input type="hidden" name="category_id"
                                       id="modal_id" value=""
                                       class="form-control"
                                       placeholder="Enter Category Name">
                            </div>
                        </div>
                        <br/>

                        <div class="form-group">
                            <label for="field-ta">Category
                                Description</label>

                            <div class="">
                            <textarea class="form-control" cols="20" rows="5" name="category_description"
                                      id="modal_description"
                                      placeholder="Enter description"></textarea>
                            </div>
                        </div>
                        <br/>

                        <div class="form-group">
                            <label>Publication
                                Status</label>

                            <div class="">
                                <select name="status" class="form-control" id="modal_status">
                                    <option value="1">
                                        Published
                                    </option>
                                    <option value="0">
                                        Pending
                                    </option>
                                </select>
                            </div>
                        </div>
                        <br/>


                    </form>


                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-icon">
                        <i class="entypo-check"></i>
                        Apply Changes
                    </button>
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">
                        Close
                    </button>
                </div>
            </div>

        </div>
    </div>


    <!-- (Normal Modal)-->
    <div class="modal fade" id="confirm-delete">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top:100px;">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align:center;">Are you sure to delete this information ?</h4>
                </div>


                <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                    <a href="" id="modal_id"
                       class="btn btn-danger btn-ok">Delete</a>
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

</div>