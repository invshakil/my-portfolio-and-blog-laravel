<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">&times;
                    </button>
                    <h4 class="modal-title">Edit
                        Education Information</h4>
                </div>
                <div class="modal-body">

                    <form role="form" class="form-horizontal form-groups-bordered" method="post"
                          action="{{ URL::to('/admin_dashboard/education/do_update/') }}"
                          enctype="multipart/form-data">

                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Session</label>

                            <div class="col-sm-5">
                                <input type="text" name="session" id="session"
                                       class="form-control"
                                       placeholder="Enter Session">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Institute Name</label>

                            <div class="col-sm-8">
                                <textarea name="institute_name" rows="3" id="institute_name" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Degree Name</label>

                            <div class="col-sm-5">
                                <input type="text" name="degree_name" id="degree_name" placeholder="Degree Name"
                                       class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Subject</label>

                            <div class="col-sm-5">
                                <input type="text" name="subject" id="subject" placeholder="Subject"
                                       class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">Result</label>

                            <div class="col-sm-5">
                                <input type="text" name="result" id="result" placeholder="Result"
                                       class="form-control"/>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Publication Status</label>

                            <div class="col-sm-5">
                                <select name="status" id="status" class="form-control">
                                    <option value="1">Published</option>
                                    <option value="0">Pending</option>
                                </select>
                            </div>
                        </div>

                        <input type="hidden" name="education_id"
                               class="form-control" id="id"
                               placeholder="Enter Title">


                    </form>


                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-icon">
                        <i class="entypo-check"></i>
                        Apply Changes
                    </button>
                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">
                        Close
                    </button>
                </div>
            </div>

        </div>
    </div>


    <!-- (Normal Modal)-->
    <div class="modal fade" id="confirm-delete">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top:100px;">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align:center;">Are you sure to delete this information ?</h4>
                </div>


                <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                    <a href="" id="id"
                       class="btn btn-danger btn-ok">Delete</a>
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

</div>