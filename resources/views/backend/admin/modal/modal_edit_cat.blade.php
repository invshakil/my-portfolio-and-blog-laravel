<?php
$edit_data = DB::table('categories')->where('category_id', $param2)->first();

?>

<div class="tab-pane box active" id="edit" style="padding: 5px">
    <div class="box-content">

        <form role="form" class="form-horizontal form-groups-bordered" method="post"
              action="{{ url('/admin_dashboard/categories/do_update') }}/{{ $edit_data->category_id }}"
              enctype="multipart/form-data">

            {!! csrf_field() !!}

            <div class="form-group">
                <label for="field-1">Category
                    Name</label>

                <div class="">
                    <input type="text" name="category_name"
                           value="{{ $edit_data->category_name }}"
                           class="form-control"
                           placeholder="Enter Category Name">

                </div>
            </div>
            <br/>

            <div class="form-group">
                <label for="field-ta">Category
                    Description</label>

                <div class="">
                            <textarea class="form-control" cols="20" rows="5" name="category_description"
                                      placeholder="Enter description">{{ $edit_data->category_description }}</textarea>
                </div>
            </div>
            <br/>

            <div class="form-group">
                <label>Publication
                    Status</label>

                <div class="">
                    <select name="status" class="form-control" id="modal_status">
                        <option value="1" @php if ($edit_data->status == 1) echo 'selected'; @endphp>
                            Published
                        </option>
                        <option value="0" @php if ($edit_data->status == 0) echo 'selected'; @endphp>
                            Pending
                        </option>
                    </select>
                </div>
            </div>



        </form>

    </div>
</div>