<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">&times;
                    </button>
                    <h4 class="modal-title">Edit
                        Article Information</h4>
                </div>
                <div class="modal-body">

                    <form role="form" class="form-horizontal form-groups-bordered" method="post"
                          action="{{ URL::to('/admin_dashboard/projects/do_update/') }}"
                          enctype="multipart/form-data">

                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Title</label>

                            <div class="col-sm-5">
                                <input type="text" name="title"
                                       class="form-control" id="title"
                                       placeholder="Enter Title">
                                <input type="hidden" name="project_id"
                                       class="form-control" id="id"
                                       placeholder="Enter Title">
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Project Featured Image</label>

                            <div class="col-sm-5">

                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail" style="width: 200px; "
                                         data-trigger="fileinput">
                                        <img src="http://placehold.it/200x150" id="featured_image">
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                         style="max-width: 200px; max-height: 150px"></div>
                                    <div>
											<span class="btn btn-white btn-file">
												<span class="fileinput-new">Select image</span>
												<span class="fileinput-exists">Change</span>
												<input type="file" name="featured_image" accept="image/*">
											</span>
                                        <a href="#" class="btn btn-orange fileinput-exists"
                                           data-dismiss="fileinput">Remove</a>
                                    </div>
                                </div>

                            </div>
                            <span style="background-color: #0a001f;color: #fff;font-weight: bold">Note: Image size maximum 300kb.</span>
                        </div>

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Description</label>

                            <div class="col-sm-8">
                                <textarea class="form-control ckeditor" name="description" id="description"
                                          placeholder="Your Content Description"></textarea>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Enter Tags</label>

                            <div class="col-sm-5">
                                <textarea type="text" id="tags" name="tags" placeholder="Tags"
                                       class="tm-input form-control tm-input-info"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="field-1" class="col-sm-3 control-label">Demo Link</label>

                            <div class="col-sm-5">
                                        <input class="form-control" name="demo_link" id="demo_link"
                                                  placeholder="Your Demo Link" />
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="col-sm-3 control-label">Visibility Status</label>

                            <div class="col-sm-5">
                                <select name="status" id="status" class="form-control">
                                    <option value="1">Published</option>
                                    <option value="0">Pending</option>
                                </select>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-success btn-icon col-md-offset-3">
                            <i class="entypo-check"></i>
                            Apply Changes
                        </button>

                    </form>


                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default"
                            data-dismiss="modal">
                        Close
                    </button>
                </div>
            </div>

        </div>
    </div>


    <!-- (Normal Modal)-->
    <div class="modal fade" id="confirm-delete">
        <div class="modal-dialog">
            <div class="modal-content" style="margin-top:100px;">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" style="text-align:center;">Are you sure to delete this information ?</h4>
                </div>


                <div class="modal-footer" style="margin:0px; border-top:0px; text-align:center;">
                    <a href="" id="id"
                       class="btn btn-danger btn-ok">Delete</a>
                    <button type="button" class="btn btn-info" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>

</div>