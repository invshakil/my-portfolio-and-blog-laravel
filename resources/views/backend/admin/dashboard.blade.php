@extends('backend.master')

@section('title')
    Admin Dashboard
@endsection

@section('main_content')
    <style>
        h3 a {
            color: white !important;
            font-size: 20px;
        }

        h3 {
            padding-bottom: 15px !important;
        }
    </style>

    <div class="row">
        <div class="col-sm-6 col-md-4 col-xs-12 col-lg-3">

            <a href="{{ route('admin.profile') }}">
                <div class="tile-title tile-ping">

                    <div class="icon">
                        <i class="glyphicon glyphicon-cog"></i>
                    </div>

                    <div class="title">
                        <h3><a href="{{ route('admin.profile') }}">Profile Settings</a></h3>
                    </div>
                </div>
            </a>

        </div>

        <div class="col-sm-6 col-md-4 col-xs-12 col-lg-3">

            <a href="{{ route('categories') }}">
                <div class="tile-title tile-red">

                    <div class="icon">
                        <i class="glyphicon glyphicon-th-large"></i>
                    </div>

                    <div class="title">
                        <h3><a href="{{ route('categories') }}">Blog Categories</a></h3>
                    </div>
                </div>

            </a>
        </div>

        <div class="col-sm-6 col-md-4 col-xs-12 col-lg-3">

            <a href="{{ route('articles') }}">
                <div class="tile-title tile-blue">

                    <div class="icon">
                        <i class="glyphicon glyphicon-edit"></i>
                    </div>

                    <div class="title">
                        <h3><a href="{{ route('articles') }}">Blog Articles</a></h3>
                    </div>
                </div>
            </a>

        </div>

        <div class="col-sm-6 col-md-4 col-xs-12 col-lg-3">

            <a href="{{ route('gallery') }}">
                <div class="tile-title tile-brown">

                    <div class="icon">
                        <i class="glyphicon glyphicon-picture"></i>
                    </div>

                    <div class="title">
                        <h3><a href="{{ route('gallery') }}">Image Gallery</a></h3>
                    </div>

                </div>
            </a>

        </div>

        <div class="col-sm-6 col-md-4 col-xs-12 col-lg-3">

            <a href="{{ route('banner') }}">
                <div class="tile-title tile-orange">

                    <div class="icon">
                        <i class="glyphicon glyphicon-info-sign"></i>
                    </div>

                    <div class="title">
                        <h3><a href="{{ route('banner') }}">Banner Information</a></h3>
                    </div>
                </div>
            </a>

        </div>

        <div class="col-sm-6 col-md-4 col-xs-12 col-lg-3">

            <a href="{{ route('workplace') }}">
                <div class="tile-title tile-green">

                    <div class="icon">
                        <i class="glyphicon glyphicon-briefcase"></i>
                    </div>

                    <div class="title">
                        <h3><a href="{{ route('workplace') }}">Workplaces</a></h3>
                    </div>
                </div>
            </a>

        </div>

        <div class="col-sm-6 col-md-4 col-xs-12 col-lg-3">

            <a href="{{ route('education') }}">
                <div class="tile-title tile-cyan">

                    <div class="icon">
                        <i class="glyphicon glyphicon-folder-open"></i>
                    </div>

                    <div class="title">
                        <h3><a href="{{ route('education') }}">Education</a></h3>
                    </div>
                </div>
            </a>

        </div>

        <div class="col-sm-6 col-md-4 col-xs-12 col-lg-3">

            <a href="{{ route('projects') }}">
                <div class="tile-title tile-purple">

                    <div class="icon">
                        <i class="glyphicon glyphicon-ok-sign"></i>
                    </div>

                    <div class="title">
                        <h3><a href="{{ route('projects') }}">Projects</a></h3>
                    </div>
                </div>
            </a>

        </div>
    </div>
@endsection