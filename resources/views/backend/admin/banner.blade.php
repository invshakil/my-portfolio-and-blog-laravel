@extends('backend.master')

@section('title')
    Banner Content
@endsection

@section('main_content')
    <div class="panel panel-dark" data-collapsed="0">

        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">Portfolio Banner Content</div>

            <div class="panel-options">
                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                            class="entypo-cog"></i></a>
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
            </div>
        </div>

        <!-- panel body -->
        <div class="panel-body">
            @if (count($errors) > 0)
                <div>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger">
                                {{ $error }}
                            </div>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="{{ URL::to('admin_dashboard/banner/do_update') }}" enctype="multipart/form-data">

                {{ csrf_field() }}

                <div class="form-group">
                    <label class="col-sm-3 control-label">Image Upload</label>

                    <div class="col-sm-5">

                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <?php $image = DB::table('banner_content')->where('info_type', 'image')->value('description'); ?>
                            <div class="fileinput-new thumbnail" style="width: 200px; height:"
                                 data-trigger="fileinput">
                                <?php if($image != ''){?>
                                <img src="<?php echo URL::to($image); ?>" alt="...">
                                <?php } else{?>
                                <img src="http://placehold.it/600x550" alt="...">
                                <?php }?>
                            </div>

                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px">

                            </div>

                            <div>
											<span class="btn btn-white btn-file">

                                                <?php if ($image != ''){?>
                                                <span class="fileinput-new">Update Image</span>
                                                <?php }else{?>
                                                <span class="fileinput-new">Select Image</span>
                                                <?php }?>

                                                <span class="fileinput-exists">Change</span>
												<input type="file" name="image" accept="image/*">
											</span>
                                <a href="#" class="btn btn-orange fileinput-exists"
                                   data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Name</label>

                    <div class="col-sm-5">
                        <?php $name = DB::table('banner_content')->where('info_type', 'name')->value('description'); ?>
                        <input type="text" name="name" class="form-control"
                               value="<?php echo $name;?>" id="field-1"
                               placeholder="Your Name">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Designation</label>

                    <div class="col-sm-5">
                        <?php $designation = DB::table('banner_content')->where('info_type', 'designation')->value('description'); ?>
                        <input type="text" name="designation" class="form-control"
                               value="<?php echo $designation;?>" id="field-1"
                               placeholder="Your Designation">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Intro Text</label>

                    <div class="col-sm-5">
                        <?php $intro_text = DB::table('banner_content')->where('info_type', 'intro_text')->value('description'); ?>
                            <textarea class="form-control" name="intro_text" id="field-ta"
                                      placeholder="Your Introduction to outside world"><?php echo $intro_text;?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Facebook Link</label>

                    <div class="col-sm-5">
                        <?php $facebook = DB::table('banner_content')->where('info_type', 'facebook')->value('description'); ?>
                        <input type="text" class="form-control" name="facebook"
                               value="<?php echo $facebook;?>" id="field-1" placeholder="Your Facebook Profile Link">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Twitter Link</label>

                    <div class="col-sm-5">
                        <?php $twitter = DB::table('banner_content')->where('info_type', 'twitter')->value('description'); ?>
                        <input type="text" class="form-control" name="twitter"
                               value="<?php echo $twitter;?>" id="field-1" placeholder="Your Twitter Profile Link">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Bitbucket Link</label>

                    <div class="col-sm-5">
                        <?php $bitbucket = DB::table('banner_content')->where('info_type', 'bitbucket')->value('description'); ?>
                        <input type="text" class="form-control" name="bitbucket"
                               value="<?php echo $bitbucket;?>" id="field-1" placeholder="Your Bitbucket Profile Link">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Stack OverFlow Link</label>

                    <div class="col-sm-5">
                        <?php $stack_overflow = DB::table('banner_content')->where('info_type', 'stack_overflow')->value('description'); ?>
                        <input type="text" class="form-control" name="stack_overflow"
                               value="<?php echo $stack_overflow;?>" id="field-1" placeholder="Your Stack Profile Link">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-success btn-icon">Save Banner Info <i class="entypo-check"></i></button>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection