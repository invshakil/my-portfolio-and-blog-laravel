@extends('backend.master')

@section('title')
    Edit Article Content
@endsection

@section('main_content')

    @php
        $data = DB::table('articles')->where('article_id',$id)->first();
        $categories = explode(',',$data->category);

    @endphp

    <form method="post" action="{{ URL::to('/admin_dashboard/articles/do_update/') }}"
          enctype="multipart/form-data" role="form">

    {{ csrf_field() }}

        <input type="hidden" class="form-control input-lg" name="article_id"
               value="{{ $data->article_id }}"/>
    <!-- Title and Publish Buttons -->
        <div class="row">
            <div class="col-sm-10">
                <input type="text" class="form-control input-lg" name="title"
                       value="{{ $data->title }}"/>
            </div>
            <div class="col-sm-2 post-save-changes">
                <button type="submit" class="btn btn-green btn-lg btn-block btn-icon">
                    Apply Changes
                    <i class="entypo-check"></i>
                </button>
            </div>
        </div>

        <br/>

        <!-- CKEditor - Content Editor -->
        <div class="row">

            <!-- Metabox :: Featured Image -->
            <div class="col-sm-3">

                <div class="panel panel-primary" data-collapsed="0">

                    <div class="panel-heading">
                        <div class="panel-title">
                            Featured Image
                        </div>

                        <div class="panel-options">
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                        </div>
                    </div>

                    <div class="panel-body">

                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail"
                                 style="max-width: 320px; height: 180px;"
                                 data-trigger="fileinput">
                                @if($data->m_size_img)
                                    <img src="{{ url($data->m_size_img) }}" alt="...">
                                @else
                                    <img src="http://placehold.it/620x348" alt="...">
                                @endif
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 320px; max-height: 180px"></div>
                            <div>
                                                    <span class="btn btn-white btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="image" accept="image/*">
                                                    </span>
                                <a href="#" class="btn btn-orange fileinput-exists"
                                   data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>

                    </div>

                </div>

            </div>
            <div class="col-sm-9">
                <textarea name="description" class="form-control" id="summernote" rows="18">{{ $data->description }} </textarea>
            </div>
        </div>

        <br/>

        <!-- Metaboxes -->
        <div class="row">

            <!-- Metabox :: Publish Settings -->
            <div class="col-sm-4">

                <div class="panel panel-primary" data-collapsed="0">

                    <div class="panel-heading">
                        <div class="panel-title">
                            Publish Settings
                        </div>

                        <div class="panel-options">
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                        </div>
                    </div>

                    <div class="panel-body">


                        <p>Slider Status</p>
                        <select name="slider_status" class="form-control">

                            <option value="1" @if($data->slider_status == 1) selected @endif>Yes</option>
                            <option value="2" @if($data->slider_status != 1) selected @endif>No</option>

                        </select>

                        <br/>

                        <p>Publication Status</p>
                        <select name="status" class="form-control">

                            <option value="1" @if($data->status == 1) selected @endif>Publish</option>
                            <option value="2" @if($data->status != 1) selected @endif>Private</option>

                        </select>

                        <br/>
                        <p>Published Date</p>
                        <div class="input-group">
                            <input type="text" class="form-control datepicker"
                                   value="{{ date('D, M/Y h:i A', strtotime($data->created_at)) }}" data-format="D, dd MM yyyy"
                                   disabled>

                            <div class="input-group-addon">
                                <a href="#"><i class="entypo-calendar"></i></a>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

            <!-- Metabox :: Categories -->
            <div class="col-sm-4">

                <div class="panel panel-primary" data-collapsed="0">

                    <div class="panel-heading">
                        <div class="panel-title">
                            Categories
                        </div>

                        <div class="panel-options">
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                        </div>
                    </div>

                    <div class="panel-body">

                        <select multiple="multiple" name="category[]"
                                class="form-control multi-select">
                            {{ $pub = DB::table('categories')->where('status',1)->get() }}
                            @foreach($pub as $row)
                                <option value="{{ $row->category_id }}"

                                <?php
                                    foreach ($categories as $category) {
                                        if ($row->category_id == $category) {
                                            echo 'selected';
                                        }
                                    }
                                    ?>

                                >{{ $row->category_name }}</option>
                            @endforeach
                        </select>

                    </div>

                </div>

            </div>

            <!-- Metabox :: Categories -->
            <div class="col-sm-4">

                <div class="panel panel-primary" data-collapsed="0">

                    <div class="panel-heading">
                        <div class="panel-title">
                            SEO Settings
                        </div>

                        <div class="panel-options">
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                        </div>
                    </div>

                    <div class="panel-body">

                        <p>Meta Title</p>
                        <textarea class="form-control" name="meta_title" id="field-ta"
                                  placeholder="Your Meta Title">{{ $data->meta_title }}</textarea>
                        <br/>

                        <p>Meta Description</p>
                        <textarea class="form-control" name="meta_description" rows="5"
                                  id="field-ta"
                                  placeholder="Your Meta Description">{{ $data->meta_description }}</textarea>

                    </div>

                </div>
            </div>


        </div>


        <div class="clear"></div>

        <!-- Metabox :: Tags -->
        <div class="row">
            <div class="col-sm-12">

                <div class="panel panel-primary" data-collapsed="0">

                    <div class="panel-heading">
                        <div class="panel-title">
                            Tags
                        </div>

                        <div class="panel-options">
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                        </div>
                    </div>

                    <div class="panel-body">

                        <p>Add Post Tags</p>
                        <input type="text" name="tags"
                               class="form-control tagsinput" value="{{ $data->tags }}"/>

                    </div>

                </div>

            </div>
        </div>


        </div>


    </form>

    <!-- include summernote css/js-->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>

    <!-- Initialize the editor. -->
    <script>
        $(document).ready(function() {
            $('#summernote').summernote();
        });

        var content = $("#summernote").val()
        console.log(content);

    </script>


    <script src="{{URL::to('/public/assets')}}/js/jquery.multi-select.js"></script>

    <script src="{{URL::to('/public/assets')}}/js/bootstrap-datepicker.js"></script>
    {{--<script src="{{URL::to('/public/assets')}}/js/selectboxit/jquery.selectBoxIt.min.js"></script>--}}
    <script src="{{URL::to('/public/assets')}}/js/bootstrap-tagsinput.min.js"></script>
@endsection