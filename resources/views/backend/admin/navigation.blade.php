<div class="sidebar-menu">

    <div class="sidebar-menu-inner">

        <header class="logo-env" style="background-color: #0a0f50; padding: 20px">

            <!-- logo -->
            <div class="logo" >
                <a href="{{ url('/blog') }}">
                    <h4 style="color: white;font-size: 17px;font-weight: bold">PORTFOLIO & BLOG</h4>
                </a>
            </div>

            <!-- logo collapse icon -->
            <div class="sidebar-collapse">
                <a href="#" class="sidebar-collapse-icon">
                    <!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition -->
                    <i class="entypo-menu"></i>
                </a>
            </div>


            <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
            <div class="sidebar-mobile-menu visible-xs">
                <a href="#" class="with-animation"><!-- add class "with-animation" to support animation -->
                    <i class="entypo-menu"></i>
                </a>
            </div>

        </header>


        <ul id="main-menu" class="main-menu">
            <!-- add class "multiple-expanded" to allow multiple submenus to open -->
            <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->

            <!-- DASHBOARD -->
            <li class="@if(Request::url() == route('admin_dashboard')) active @endif" >
                <a href="{{ URL::to('admin_dashboard') }}" >
                    <i class="fa fa-home fa-3x"></i>
                    <span>Dashboard</span>
                </a>
            </li>

            <!-- PROFILE -->
            <li class="@if(Request::url() == route('admin.profile')) active @endif" >
                <a href="{{ URL::to('admin_dashboard/profile') }}" >
                    <i class="fa fa-gears fa-3x"></i>
                    <span>Profile</span>
                </a>
            </li>

            <!-- BOOKS -->
            <li class="@if(Request::url() == route('categories') || Request::url() == route('articles')) opened active has-sub @endif">
                <a href="#">
                    <i class="fa fa-edit fa-3x"></i>
                    <span>Blog Management</span>
                </a>
                <ul>
                    <li class="@if(Request::url() == route('categories')) active @endif" >
                        <a href="{{ URL::to('admin_dashboard/categories') }}" >
                            <i class="fa fa-th-large fa-3x"></i>
                            <span>Blog Category</span>
                        </a>
                    </li>
                    <li class="@if(Request::url() == route('articles')) active @endif" >
                        <a href="{{ URL::to('admin_dashboard/articles') }}" >
                            <i class="fa fa-pencil-square-o fa-3x"></i>
                            <span>Blog Article</span>
                        </a>
                    </li>
                </ul>
            </li>

            <li class="@if(Request::url() == route('gallery')) active @endif" >
                <a href="{{ URL::to('admin_dashboard/uploaded_image') }}" >
                    <i class="fa fa-picture-o fa-3x"></i>
                    <span>Uploaded Image</span>
                </a>
            </li>

            <li class="@if(Request::url() == route('banner')) active @endif" >
                <a href="{{ URL::to('admin_dashboard/banner') }}" >
                    <i class="fa fa-info-circle fa-3x"></i>
                    <span>Banner Content</span>
                </a>
            </li>

            <li class="@if(Request::url() == route('about')) active @endif" >
                <a href="{{ URL::to('admin_dashboard/about') }}" >
                    <i class="fa fa-dot-circle-o fa-3x"></i>
                    <span>About Content</span>
                </a>
            </li>

            {{--<li class="@if(Request::url() == route('skill.set')) active @endif" >--}}
                {{--<a href="{{ URL::to('admin_dashboard/skill_set') }}" >--}}
                    {{--<i class="fa fa-hand-o-right fa-3x"></i>--}}
                    {{--<span>Skill Set</span>--}}
                {{--</a>--}}
            {{--</li>--}}

            <li class="@if(Request::url() == route('workplace')) active @endif" >
                <a href="{{ URL::to('admin_dashboard/workplace') }}" >
                    <i class="fa fa-briefcase fa-3x"></i>
                    <span>Workplace</span>
                </a>
            </li>

            <li class="@if(Request::url() == route('education')) active @endif" >
                <a href="{{ URL::to('admin_dashboard/education') }}" >
                    <i class="fa fa-graduation-cap fa-3x"></i>
                    <span>Education</span>
                </a>
            </li>

            {{--<li class="@if(Request::url() == route('services')) active @endif" >--}}
                {{--<a href="{{ URL::to('admin_dashboard/services') }}" >--}}
                    {{--<i class="fa fa-hand-o-right fa-3x"></i>--}}
                    {{--<span>My Services</span>--}}
                {{--</a>--}}
            {{--</li>--}}

            <li class="@if(Request::url() == route('projects')) active @endif" >
                <a href="{{ URL::to('admin_dashboard/projects') }}" >
                    <i class="fa fa-code fa-3x"></i>
                    <span>My Projects</span>
                </a>
            </li>

            {{--<li class="@if(Request::url() == route('contact')) active @endif" >--}}
                {{--<a href="{{ URL::to('admin_dashboard/contact') }}" >--}}
                    {{--<i class="fa fa-hand-o-right fa-3x"></i>--}}
                    {{--<span>Contact Page</span>--}}
                {{--</a>--}}
            {{--</li>--}}


        </ul>

    </div>

</div>