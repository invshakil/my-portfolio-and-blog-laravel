@extends('backend.master')

@section('title')
    Article Publish
@endsection

@section('main_content')

    <div class="panel minimal minimal-gray">

        @if (count($errors) > 0)
            <div>
                <ul>
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">
                            {{ $error }}
                        </div>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="panel-heading">
            <div class="panel-title"><h2>Article Add/Manage</h2></div>
            <div class="panel-options">

                <ul class="nav nav-tabs">
                    <li class="active"><a href="#profile-1" data-toggle="tab">Article List</a></li>
                    <li><a href="#profile-2" data-toggle="tab">Add Article</a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">

            <div class="tab-content">
                <div class="tab-pane active" id="profile-1">
                    <div class="panel panel-dark" data-collapsed="0">
                        <!-- panel head -->
                        <div class="panel-heading">
                            <div class="panel-title">Article List</div>

                            <div class="panel-options">
                                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1"
                                   class="bg"><i
                                            class="entypo-cog"></i></a>
                                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                            </div>
                        </div>

                        <!-- panel body -->
                        <div class="panel-body table-responsive">

                            <table class="table table-bordered datatable" id="table-1">
                                <thead>
                                <tr>
                                    <th width="5%">ID</th>
                                    <th width="10%">Image</th>
                                    <th width="15%">Title</th>
                                    <th width="15%">Category</th>
                                    <th width="25%">Tags</th>
                                    <th width="8%">Author</th>
                                    <th width="7%">Status</th>
                                    <th width="12%">Options</th>
                                </tr>
                                </thead>
                                <tbody>
								<?php $result = DB ::table('articles')
									-> join('users', 'articles.author', '=', 'users.id')
									-> select('articles.*', 'users.name')
									-> get();?>

                                @foreach($result->all() as $row)
                                    <tr>

                                        <td class="center">{{ $row->article_id }}</td>
                                        <td class="center"><img src="{{ url($row->thumbnail) }}" width="50"></td>
                                        <td class="center">
                                            @if($row->status==1)
                                                <a target="_blank" href="{{ url('/blog/article'.'/'.$row->article_id.'/'.str_replace(' ','-',$row->title) ) }}">
                                                    {{ $row->title }}
                                                </a>
                                            @else
                                                {{ $row->title }}
                                            @endif
                                        </td>
                                        <td class="center">

                                            @php $categories = explode(',',$row->category); @endphp
                                            @foreach($categories as $category)
                                                <div class="label label-info">
                                                    @php $cat_name = DB::table('categories')->where('category_id',$category)->value('category_name') @endphp
                                                    {{ $cat_name }}
                                                </div>
                                            @endforeach
                                        </td>
                                        <td class="center">
                                            @php $tags = explode(',',$row->tags) @endphp
                                            @foreach($tags as $tag)
                                                <button class="btn btn-info btn-sm">{{ $tag }}</button>
                                            @endforeach
                                        </td>
                                        <td class="center">
                                            <div class="label label-primary">{{ $row->name }}</div>
                                        </td>
                                        <td class="center">
                                            @if($row->status==1)
                                                <div class="label label-success">Published</div>
                                            @else
                                                <div class="label label-danger">Pending</div>
                                            @endif
                                        </td>
                                        <td class="center">

                                            <a href="{{ url('/admin_dashboard/articles/'.$row->article_id) }}"
                                               class="btn btn-success btn-icon pull-left"
                                            >
                                                Edit
                                                <i class="fa fa-trash-o"></i>
                                            </a>

                                            <a href="#" class="btn btn-danger btn-icon pull-right"
                                               data-toggle="modal" data-id="{{$row->article_id}}"
                                               data-target="#confirm-delete">
                                                Delete
                                                <i class="fa fa-trash-o"></i>
                                            </a>

                                        </td>
                                    </tr>


                                @endforeach

                                </tbody>

                            </table>
                        </div>
                    </div>


                </div>

                <div class="tab-pane" id="profile-2">
                    <div class="panel panel-dark" data-collapsed="0">
                        <!-- panel head -->
                        <div class="panel-heading">
                            <div class="panel-title">Add Article</div>

                            <div class="panel-options">
                                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1"
                                   class="bg"><i
                                            class="entypo-cog"></i></a>
                                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                            </div>
                        </div>

                        <!-- panel body -->
                        <div class="panel-body">

                            <style>
                                .ms-container .ms-list {
                                    width: 135px;
                                    height: 205px;
                                }

                                .post-save-changes {
                                    float: right;
                                }

                                @media screen and (max-width: 789px) {
                                    .post-save-changes {
                                        float: none;
                                        margin-bottom: 20px;
                                    }
                                }
                            </style>

                            <form method="post" action="{{ URL::to('/admin_dashboard/articles/create') }}"
                                  enctype="multipart/form-data" role="form">

                            {{ csrf_field() }}

                            <!-- Title and Publish Buttons -->
                                <div class="row">
                                    <div class="col-sm-2 post-save-changes">
                                        <button type="submit" class="btn btn-green btn-lg btn-block btn-icon">
                                            Publish
                                            <i class="entypo-check"></i>
                                        </button>
                                    </div>

                                    <div class="col-sm-10">
                                        <input type="text" class="form-control input-lg" name="title"
                                               placeholder="Article title"/>
                                    </div>
                                </div>

                                <br/>

                                <!-- CKEditor - Content Editor -->
                                <div class="row">
                                    <div class="col-sm-12">
                                        <textarea name="description" class="form-control ckeditor" rows="18"></textarea>
                                    </div>
                                </div>

                                <br/>

                                <!-- Metaboxes -->
                                <div class="row">

                                    <!-- Metabox :: Publish Settings -->
                                    <div class="col-sm-3">

                                        <div class="panel panel-primary" data-collapsed="0">

                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    Publish Settings
                                                </div>

                                                <div class="panel-options">
                                                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                                </div>
                                            </div>

                                            <div class="panel-body">


                                                <p>Slider Status</p>
                                                <select name="slider_status" class="form-control">

                                                    <option value="1">Yes</option>
                                                    <option value="2">No</option>

                                                </select>

                                                <br/>

                                                <p>Publication Status</p>
                                                <select name="status" class="form-control">

                                                    <option value="1">Publish</option>
                                                    <option value="2">Private</option>

                                                </select>

                                                <br/>
                                                <p>Publish Date</p>
                                                <div class="input-group">
                                                    <input type="text" class="form-control datepicker"
                                                           value="Tue, 09 December 2014" data-format="D, dd MM yyyy"
                                                           disabled>

                                                    <div class="input-group-addon">
                                                        <a href="#"><i class="entypo-calendar"></i></a>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>

                                    </div>


                                    <!-- Metabox :: Featured Image -->
                                    <div class="col-sm-3">

                                        <div class="panel panel-primary" data-collapsed="0">

                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    Featured Image
                                                </div>

                                                <div class="panel-options">
                                                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                                </div>
                                            </div>

                                            <div class="panel-body">

                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail"
                                                         style="max-width: 320px; height: 180px;"
                                                         data-trigger="fileinput">
                                                        <img src="http://placehold.it/620x348" alt="...">
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail"
                                                         style="max-width: 320px; max-height: 180px"></div>
                                                    <div>
                                                    <span class="btn btn-white btn-file">
                                                        <span class="fileinput-new">Select image</span>
                                                        <span class="fileinput-exists">Change</span>
                                                        <input type="file" name="image" accept="image/*">
                                                    </span>
                                                        <a href="#" class="btn btn-orange fileinput-exists"
                                                           data-dismiss="fileinput">Remove</a>
                                                    </div>
                                                </div>

                                                OR <br/><br/>
                                                <input type="text" name="image" class="form-control" placeholder="Enter Image Path">

                                            </div>

                                        </div>

                                    </div>


                                    <!-- Metabox :: Categories -->
                                    <div class="col-sm-3">

                                        <div class="panel panel-primary" data-collapsed="0">

                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    Categories
                                                </div>

                                                <div class="panel-options">
                                                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                                </div>
                                            </div>

                                            <div class="panel-body">

                                                <select multiple="multiple" name="category[]"
                                                        class="form-control multi-select">
                                                    {{ $pub = DB::table('categories')->where('status',1)->get() }}
                                                    @foreach($pub as $row)
                                                        <option value="{{ $row->category_id }}">{{ $row->category_name }}</option>
                                                    @endforeach
                                                </select>

                                            </div>

                                        </div>

                                    </div>

                                    <!-- Metabox :: Categories -->
                                    <div class="col-sm-3">

                                        <div class="panel panel-primary" data-collapsed="0">

                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    SEO Settings
                                                </div>

                                                <div class="panel-options">
                                                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                                </div>
                                            </div>

                                            <div class="panel-body">

                                                <p>Meta Title</p>
                                                <textarea class="form-control" name="meta_title" id="field-ta"
                                                          placeholder="Your Meta Title"></textarea>
                                                <br/>

                                                <p>Meta Description</p>
                                                <textarea class="form-control" name="meta_description" rows="5"
                                                          id="field-ta"
                                                          placeholder="Your Meta Description"></textarea>

                                            </div>

                                        </div>
                                    </div>

                                    <div class="clear"></div>

                                    <!-- Metabox :: Tags -->
                                    <div class="col-sm-12">

                                        <div class="panel panel-primary" data-collapsed="0">

                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    Tags
                                                </div>

                                                <div class="panel-options">
                                                    <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                                </div>
                                            </div>

                                            <div class="panel-body">

                                                <p>Add Post Tags</p>
                                                <input type="text" name="tags"
                                                       class="form-control tagsinput"/>

                                            </div>

                                        </div>

                                    </div>

                                </div>


                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>


    </div>

    <style>
        .panel .panel-primary > .panel-heading {
            background-color: #0a0f50;
            color: white;
        }
    </style>

    <script type="text/javascript">
        $('form input').on('keypress', function (e) {
            return e.which !== 13;
        });

        var responsiveHelper;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        var tableContainer;

        jQuery(document).ready(function ($) {
            tableContainer = $("#table-1");

            tableContainer.dataTable({
                "sPaginationType": "bootstrap",
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bStateSave": true,


                // Responsive Settings
                bAutoWidth: false,
                fnPreDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper) {
                        responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                    }
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    responsiveHelper.createExpandIcon(nRow);
                },
                fnDrawCallback: function (oSettings) {
                    responsiveHelper.respond();
                }
            });

            $(".dataTables_wrapper select").select2({
                minimumResultsForSearch: -1
            });
        });

    </script>

    @include('backend.admin.modal.modal_edit_articles')

    <script type="text/javascript">

        $('#confirm-delete').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id');

            $('a#id').attr('href', '{{URL::to('/admin_dashboard/articles/delete')}}/' + id);
        });


    </script>


    <script src="{{URL::to('/public/assets')}}/js/jquery.multi-select.js"></script>

    <script src="{{URL::to('/public/assets')}}/js/bootstrap-datepicker.js"></script>
    {{--<script src="{{URL::to('/public/assets')}}/js/selectboxit/jquery.selectBoxIt.min.js"></script>--}}
    <script src="{{URL::to('/public/assets')}}/js/bootstrap-tagsinput.min.js"></script>


@endsection