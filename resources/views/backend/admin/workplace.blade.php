@extends('backend.master')

@section('title')
    Workplace Management
@endsection

@section('main_content')
    <script src="{{url('public')}}/assets/js/bootstrap-tagsinput.min.js" xmlns="http://www.w3.org/1999/html"></script>

    <div class="panel minimal minimal-gray">

        @if (count($errors) > 0)
            <div>
                <ul>
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">
                            {{ $error }}
                        </div>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="panel-heading">
            <div class="panel-title"><h2>Workplace Add/Manage</h2></div>
            <div class="panel-options">

                <ul class="nav nav-tabs">
                    <li class="active"><a href="#profile-1" data-toggle="tab">Workplace List</a></li>
                    <li><a href="#profile-2" data-toggle="tab">Add Workplace</a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">

            <div class="tab-content">
                <div class="tab-pane active" id="profile-1">
                    <div class="panel panel-dark" data-collapsed="0">
                        <!-- panel head -->
                        <div class="panel-heading">
                            <div class="panel-title">Workplace List</div>

                            <div class="panel-options">
                                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1"
                                   class="bg"><i
                                            class="entypo-cog"></i></a>
                                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                            </div>
                        </div>

                        <!-- panel body -->
                        <div class="panel-body table-responsive">

                            <table class="table table-bordered datatable" id="table-1">
                                <thead>
                                <tr>
                                    <th width="10%">ID</th>
                                    <th width="20%">Duration</th>
                                    <th width="20%">Company</th>
                                    <th width="20%">Designation</th>
                                    <th width="20%">Language</th>
                                    <th width="10%">Description</th>
                                    <th width="10%">Status</th>
                                    <th width="10%">Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $result = DB::table('workplace')
                                    ->get();?>

                                @foreach($result->all() as $row)
                                    <tr>

                                        <td class="center">{{ $row->workplace_id }}</td>
                                        <td class="center">{{ $row->duration }}</td>
                                        <td class="center">
                                            <div class="label label-info">{{ $row->company_name }}</div>
                                        </td>
                                        <td class="center">{{ $row->designation }}</td>
                                        <td class="center">{{ $row->language }}</td>
                                        <td class="center">{{ $row->description }}</td>

                                        <td class="center">
                                            @if($row->status==1)
                                                <div class="label label-success">Published</div>
                                            @else
                                                <div class="label label-danger">Pending</div>
                                            @endif
                                        </td>
                                        <td class="center">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default btn-sm dropdown-toggle"
                                                        data-toggle="dropdown">
                                                    Action <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                    <!-- EDITING LINK -->
                                                    <li>
                                                        <a href="#"
                                                           data-id="{{ $row->workplace_id }}"
                                                           data-duration="{{ $row->duration }}"
                                                           data-company_name="{{ $row->company_name }}"
                                                           data-designation="{{ $row->designation }}"
                                                           data-language="{{ $row->language }}"
                                                           data-description="{{ $row->description }}"
                                                           data-status="{{ $row->status }}"
                                                           data-toggle="modal"
                                                           data-target="#updateModal"
                                                        >
                                                            <i class="fa fa-pencil-square-o"></i>
                                                            edit
                                                        </a>
                                                    </li>
                                                    <li class="divider"></li>

                                                    <!-- DELETION LINK -->
                                                    <li>
                                                        <a href="#" data-toggle="modal" data-id="{{$row->workplace_id}}"
                                                           data-target="#confirm-delete">
                                                            <i class="fa fa-trash-o"></i>
                                                            delete
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>


                                @endforeach

                                </tbody>

                            </table>
                        </div>
                    </div>


                </div>

                <div class="tab-pane" id="profile-2">
                    <div class="panel panel-dark" data-collapsed="0">
                        <!-- panel head -->
                        <div class="panel-heading">
                            <div class="panel-title">Add Workplace</div>

                            <div class="panel-options">
                                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1"
                                   class="bg"><i
                                            class="entypo-cog"></i></a>
                                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                            </div>
                        </div>

                        <!-- panel body -->
                        <div class="panel-body">

                            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                                  action="{{ URL::to('/admin_dashboard/workplace/create') }}"
                                  enctype="multipart/form-data">

                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Duration</label>

                                    <div class="col-sm-5">
                                        <input type="text" name="duration"
                                               class="form-control" id="field-1"
                                               placeholder="Enter Duration">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Company Name</label>

                                    <div class="col-sm-8">
                                        <textarea name="company_name" rows="3" class="form-control"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Designation</label>

                                    <div class="col-sm-5">
                                        <input type="text" name="designation" placeholder="Designation"
                                               class="form-control"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Language</label>

                                    <div class="col-sm-5">
                                        <input type="text" name="language" placeholder="Language"
                                               class="form-control"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Description</label>

                                    <div class="col-sm-9">
                                        <textarea type="text" name="description" rows="5"
                                                  class="form-control"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Publication Status</label>

                                    <div class="col-sm-5">
                                        <select name="status" class="form-control">
                                            <option value="1">Published</option>
                                            <option value="0">Pending</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" id="submit" class="btn btn-primary btn-icon">Add Work Place <i class="entypo-check"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>


    </div>

    <script type="text/javascript">
        $('form input').on('keypress', function (e) {
            return e.which !== 13;
        });

        var responsiveHelper;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        var tableContainer;

        jQuery(document).ready(function ($) {
            tableContainer = $("#table-1");

            tableContainer.dataTable({
                "sPaginationType": "bootstrap",
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bStateSave": true,


                // Responsive Settings
                bAutoWidth: false,
                fnPreDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper) {
                        responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                    }
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    responsiveHelper.createExpandIcon(nRow);
                },
                fnDrawCallback: function (oSettings) {
                    responsiveHelper.respond();
                }
            });

            $(".dataTables_wrapper select").select2({
                minimumResultsForSearch: -1
            });
        });

    </script>

    @include('backend.admin.modal.modal_edit_workplace')

    <script type="text/javascript">
        $('#updateModal').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id'); //<- It's data-id value
            var duration = $(e.relatedTarget).data('duration'); //<- It's data-name value
            var company_name = $(e.relatedTarget).data('company_name'); //<- It's data-description value
            var description = $(e.relatedTarget).data('description'); //<- It's data-description value
            var designation = $(e.relatedTarget).data('designation'); //<- It's data-status value
            var language = $(e.relatedTarget).data('language'); //<- It's data-status value
            var status = $(e.relatedTarget).data('status'); //<- It's data-status value

            $('#id').val(id);
            $('#duration').val(duration);
            $('textarea#company_name').val(company_name);

            $('textarea#description').val(description);

            $('#designation').val(designation);
            $('#language').val(language);

            $('select#status').val(status);

        });

        $('#confirm-delete').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id'); //<- It's data-id value

            $('a#id').attr('href', '{{URL::to('/admin_dashboard/workplace/delete')}}/' + id);
        });
    </script>



@endsection