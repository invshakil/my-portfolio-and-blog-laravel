@extends('backend.master')

@section('title')
    About Content
@endsection

@section('main_content')
    <div class="panel panel-dark" data-collapsed="0">

        <!-- panel head -->
        <div class="panel-heading">
            <div class="panel-title">Portfolio About Content</div>

            <div class="panel-options">
                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1" class="bg"><i
                            class="entypo-cog"></i></a>
                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
            </div>
        </div>

        <!-- panel body -->
        <div class="panel-body">
            @if (count($errors) > 0)
                <div>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger">
                                {{ $error }}
                            </div>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                  action="{{ URL::to('admin_dashboard/about/do_update') }}" enctype="multipart/form-data">

                {{ csrf_field() }}

                <div class="form-group">
                    <label class="col-sm-3 control-label">Image Upload</label>

                    <div class="col-sm-5">

                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <?php $image = DB::table('about_content')->where('info_type', 'image')->value('description'); ?>
                            <div class="fileinput-new thumbnail" style="width: 200px;"
                                 data-trigger="fileinput">
                                <?php if($image != ''){?>
                                <img src="<?php echo URL::to($image); ?>" alt="...">
                                <?php } else{?>
                                <img src="http://placehold.it/600x800" alt="...">
                                <?php }?>
                            </div>

                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px">

                            </div>

                            <div>
											<span class="btn btn-white btn-file">

                                                <?php if ($image != ''){?>
                                                <span class="fileinput-new">Update Image</span>
                                                <?php }else{?>
                                                <span class="fileinput-new">Select Image</span>
                                                <?php }?>

                                                <span class="fileinput-exists">Change</span>
												<input type="file" name="image" accept="image/*">
											</span>
                                <a href="#" class="btn btn-orange fileinput-exists"
                                   data-dismiss="fileinput">Remove</a>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Name</label>

                    <div class="col-sm-5">
                        <?php $name = DB::table('about_content')->where('info_type', 'name')->value('description'); ?>
                        <input type="text" name="name" class="form-control"
                               value="<?php echo $name;?>" id="field-1"
                               placeholder="Your Name">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Age</label>

                    <div class="col-sm-5">
                        <?php $age = DB::table('about_content')->where('info_type', 'age')->value('description'); ?>
                        <input type="text" name="age" class="form-control"
                               value="<?php echo $age;?>" id="field-1"
                               placeholder="Your age">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">About Yourself</label>

                    <div class="col-sm-5">
                        <?php $about = DB::table('about_content')->where('info_type', 'about')->value('description'); ?>
                        <textarea class="form-control" name="about" id="field-ta" rows="5"
                                  placeholder="About Yourself"><?php echo $about;?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Professional Experience</label>

                    <div class="col-sm-5">
                        <?php $experience = DB::table('about_content')->where('info_type', 'experience')->value('description'); ?>
                        <input type="text" class="form-control" name="experience"
                               value="<?php echo $experience;?>" id="field-1" placeholder="Your Professional Experience">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Email</label>

                    <div class="col-sm-5">
                        <?php $email = DB::table('about_content')->where('info_type', 'email')->value('description'); ?>
                        <input type="text" class="form-control" name="email"
                               value="<?php echo $email;?>" id="field-1" placeholder="Your Email">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Phone Number</label>

                    <div class="col-sm-5">
                        <?php $phone = DB::table('about_content')->where('info_type', 'phone')->value('description'); ?>
                        <input type="text" class="form-control" name="phone"
                               value="<?php echo $phone;?>" id="field-1" placeholder="Your Phone Number">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Address</label>

                    <div class="col-sm-5">
                        <?php $address = DB::table('about_content')->where('info_type', 'address')->value('description'); ?>
                        <input type="text" class="form-control" name="address"
                               value="<?php echo $address;?>" id="field-1" placeholder="Your Address">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Website</label>

                    <div class="col-sm-5">
                        <?php $website = DB::table('about_content')->where('info_type', 'website')->value('description'); ?>
                        <input type="text" class="form-control" name="website"
                               value="<?php echo $website;?>" id="field-1" placeholder="Your Website">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">CV Link (GOOGLE DRIVE)</label>

                    <div class="col-sm-5">
                        <?php $cv = DB::table('about_content')->where('info_type', 'cv')->value('description'); ?>
                        <input type="text" class="form-control" name="cv"
                               value="<?php echo $cv;?>" id="field-1" placeholder="Your CV link">
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Blog Address</label>

                    <div class="col-sm-5">
                        <?php $blog = DB::table('about_content')->where('info_type', 'blog')->value('description'); ?>
                        <input type="text" class="form-control" name="blog"
                               value="<?php echo $blog;?>" id="field-1" placeholder="Your Blog Address">
                    </div>
                </div>


                <div class="form-group">
                    <label for="field-1" class="col-sm-3 control-label">Video (ifrmae link)</label>

                    <div class="col-sm-5">
                        <?php $video = DB::table('about_content')->where('info_type', 'video')->value('description'); ?>
                        <input type="text" class="form-control" name="video"
                               value="<?php echo $video;?>" id="field-1" placeholder="Your Video Link">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-5">
                        <button type="submit" class="btn btn-success btn-icon">Save About Info <i class="entypo-check"></i></button>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection