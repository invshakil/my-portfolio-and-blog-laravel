@extends('backend.master')

@section('title')
    Categories
@endsection

@section('main_content')
    <div class="panel minimal minimal-gray">

        @if (count($errors) > 0)
            <div>
                <ul>
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger">
                            {{ $error }}
                        </div>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="panel-heading">
            <div class="panel-title"><h2>Book Category Add/Manage</h2></div>
            <div class="panel-options">

                <ul class="nav nav-tabs">
                    <li class="active"><a href="#profile-1" data-toggle="tab">Category List</a></li>
                    <li><a href="#profile-2" data-toggle="tab">Add Category</a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">

            <div class="tab-content">
                <div class="tab-pane active" id="profile-1">
                    <div class="panel panel-dark" data-collapsed="0">
                        <!-- panel head -->
                        <div class="panel-heading">
                            <div class="panel-title">Category List</div>

                            <div class="panel-options">
                                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1"
                                   class="bg"><i
                                            class="entypo-cog"></i></a>
                                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                            </div>
                        </div>

                        <!-- panel body -->
                        <div class="panel-body table-responsive">

                            <table class="table table-bordered datatable" id="table-1">
                                <thead>
                                <tr>
                                    <th width="10%">ID</th>
                                    <th width="20%">Name</th>
                                    <th width="40%">Description</th>
                                    <th width="15%">Status</th>
                                    <th width="15%">Options</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $result = DB::table('categories')->get();?>

                                @foreach($result->all() as $row)
                                    <tr>

                                        <td class="center">{{ $row->category_id }}</td>
                                        <td class="center">{{ $row->category_name }}</td>
                                        <td>{{ $row->category_description }}</td>
                                        <td class="center">
                                            @if($row->status==1)
                                                <div class="label label-success">Published</div>
                                            @else
                                                <div class="label label-danger">Pending</div>
                                            @endif
                                        </td>
                                        <td class="center">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-default btn-sm dropdown-toggle"
                                                        data-toggle="dropdown">
                                                    Action <span class="caret"></span>
                                                </button>
                                                <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                                                    <!-- EDITING LINK -->
                                                    <li>
                                                        <a href="#" data-id="{{ $row->category_id }}"
                                                           data-name="{{ $row->category_name }}"
                                                           data-description="{{ $row->category_description }}"
                                                           data-status="{{ $row->status }}"
                                                           data-toggle="modal" data-target="#updateModal">
                                                            <i class="fa fa-pencil-square-o"></i>
                                                            edit
                                                        </a>
                                                    </li>
                                                    <li class="divider"></li>

                                                    <!-- DELETION LINK -->
                                                    <li>
                                                        <a href="#" data-toggle="modal" data-id="{{$row->category_id}}"
                                                           data-target="#confirm-delete">
                                                            <i class="fa fa-trash-o"></i>
                                                            delete
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </td>
                                    </tr>


                                @endforeach

                                </tbody>

                            </table>
                        </div>
                    </div>


                </div>

                <div class="tab-pane" id="profile-2">
                    <div class="panel panel-dark" data-collapsed="0">
                        <!-- panel head -->
                        <div class="panel-heading">
                            <div class="panel-title">Add Category</div>

                            <div class="panel-options">
                                <a href="#sample-modal" data-toggle="modal" data-target="#sample-modal-dialog-1"
                                   class="bg"><i
                                            class="entypo-cog"></i></a>
                                <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                                <a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
                                <a href="#" data-rel="close"><i class="entypo-cancel"></i></a>
                            </div>
                        </div>

                        <!-- panel body -->
                        <div class="panel-body">

                            <form role="form" class="form-horizontal form-groups-bordered" method="post"
                                  action="{{ URL::to('/admin_dashboard/categories/create') }}"
                                  enctype="multipart/form-data">

                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="field-1" class="col-sm-3 control-label">Category Name</label>

                                    <div class="col-sm-5">
                                        <input type="text" name="category_name" class="form-control" id="field-1"
                                               placeholder="Enter Category Name">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="field-ta" class="col-sm-3 control-label">Category Description</label>

                                    <div class="col-sm-5">
                                        <textarea class="form-control" name="category_description" id="field-ta"
                                                  placeholder="Enter description"></textarea>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Publication Status</label>

                                    <div class="col-sm-5">
                                        <select name="status" class="form-control">
                                            <option value="1">Published</option>
                                            <option value="0">Pending</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-5">
                                        <button type="submit" class="btn btn-primary btn-icon">Add Category <i class="entypo-check"></i></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>


    </div>

    <script type="text/javascript">
        var responsiveHelper;
        var breakpointDefinition = {
            tablet: 1024,
            phone: 480
        };
        var tableContainer;

        jQuery(document).ready(function ($) {
            tableContainer = $("#table-1");

            tableContainer.dataTable({
                "sPaginationType": "bootstrap",
                "aLengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
                "bStateSave": true,


                // Responsive Settings
                bAutoWidth: false,
                fnPreDrawCallback: function () {
                    // Initialize the responsive datatables helper once.
                    if (!responsiveHelper) {
                        responsiveHelper = new ResponsiveDatatablesHelper(tableContainer, breakpointDefinition);
                    }
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    responsiveHelper.createExpandIcon(nRow);
                },
                fnDrawCallback: function (oSettings) {
                    responsiveHelper.respond();
                }
            });

            $(".dataTables_wrapper select").select2({
                minimumResultsForSearch: -1
            });
        });

    </script>

    @include('backend.admin.modal.modal_edit_category')

    <script type="text/javascript">

        $('#updateModal').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id'); //<- It's data-id value
            var name = $(e.relatedTarget).data('name'); //<- It's data-name value
            var desc = $(e.relatedTarget).data('description'); //<- It's data-description value
            var status = $(e.relatedTarget).data('status'); //<- It's data-status value

            $('#modal_id').val(id);
            $('#modal_name').val(name);
            $('textarea#modal_description').val(desc);
            $('select#modal_status').val(status);
        });

        $('#confirm-delete').on('show.bs.modal', function (e) {
            var id = $(e.relatedTarget).data('id'); //<- It's data-id value

            $('a#modal_id').attr('href','{{URL::to('/admin_dashboard/categories/delete')}}/'+id);
        });
    </script>


@endsection