@extends('backend.master')

@section('title')
    Image Gallery
@endsection

@section('main_content')

    <script type="text/javascript">
        jQuery(document).ready(function($)
        {
            $(".gallery-env").on("click", ".image-thumb .image-options a.delete", function(ev)
            {
                ev.preventDefault();


                var $image = $(this).closest('[data-tag]');

                var t = new TimelineLite({
                    onComplete: function()
                    {
                        $image.slideUp(function()
                        {
                            $image.remove();
                        });
                    }
                });

                $image.addClass('no-animation');

                t.append( TweenMax.to($image, .2, {css: {scale: 0.95}}) );
                t.append( TweenMax.to($image, .5, {css: {autoAlpha: 0, transform: "translateX(100px) scale(.95)"}}) );

            }).on("click", ".image-thumb .image-options a.edit", function(ev)
            {
                ev.preventDefault();

                // This will open sample modal
                $("#album-image-options").modal('show');

                // Sample Crop Instance
                var image_to_crop = $("#album-image-options img"),
                    img_load = new Image();

                img_load.src = image_to_crop.attr('src');
                img_load.onload = function()
                {
                    if(image_to_crop.data('loaded'))
                        return false;

                    image_to_crop.data('loaded', true);

                    image_to_crop.Jcrop({
                        //boxWidth: $("#album-image-options").outerWidth(),
                        boxWidth: 580,
                        boxHeight: 385,
                        onSelect: function(cords)
                        {
                            // you can use these vars to save cropping of the image coordinates
                            var h = cords.h,
                                w = cords.w,

                                x1 = cords.x,
                                x2 = cords.x2,

                                y1 = cords.w,
                                y2 = cords.y2;

                        }
                    }, function()
                    {
                        var jcrop = this;

                        jcrop.animateTo([600, 400, 100, 150]);
                    });
                }
            });


            // Sample Filtering
            var all_items = $("div[data-tag]"),
                categories_links = $(".image-categories a");

            categories_links.click(function(ev)
            {
                ev.preventDefault();

                var $this = $(this),
                    filter = $this.data('filter');

                categories_links.removeClass('active');
                $this.addClass('active');

                all_items.addClass('not-in-filter').filter('[data-tag="' + filter + '"]').removeClass('not-in-filter');

                if(filter == 'all' || filter == '*')
                {
                    all_items.removeClass('not-in-filter');
                    return;
                }
            });

        });
    </script>

    <div class="gallery-env">

        <div class="row">

            <div class="col-sm-12">

                <h3>
                    Album Title
                    &nbsp;
                    <a href="#" onclick="jQuery('#album-cover-options').modal('show');" class="btn btn-default btn-sm btn-icon icon-left">
                        <i class="entypo-cog"></i>
                        Edit Album
                    </a>
                </h3>

                <hr />

                <div class="image-categories">
                    <span>Filter Images:</span>
                    <a href="#" class="active" data-filter="all">Show All</a> /
                    <a href="#" data-filter="1d">Taken today</a> /
                    <a href="#" data-filter="3d">Taken three days ago</a> /
                    <a href="#" data-filter="1w">Taken a week ago</a>
                </div>
            </div>

        </div>

        <div class="row">

            @php
                $images = DB::table('articles')->select('image','article_id')->get();
                    @endphp

            @foreach($images as $image)

            <div class="col-sm-3 col-xs-6" data-tag="1d" style="height: 400px;">

                <article class="image-thumb album">

                    <a href="#" class="image">
                        <img src="{{URL::to($image->image)}}" />
                    </a>

                    <div class="image-options">
                        <a href="#" class="edit"><i class="entypo-pencil"></i></a>
                        <a href="#" class="delete"><i class="entypo-cancel"></i></a>
                    </div>

                    <section class="album-info">
                        <p style="display: none">Image Path: <em id="image{{ $image->article_id }}">{{ $image->image }} </em></p>
                        <br/>
                        <button onclick="copyToClipboard('#image{{ $image->article_id }}')" class="btn btn-danger">Copy Image Source</button>
                    </section>

                </article>



            </div>

                @endforeach

        </div>

    </div>

    <script>
        function copyToClipboard(element) {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($(element).text()).select();
            document.execCommand("copy");
            $temp.remove();
            alert('Image Source Copied to clipboard');
        }

    </script>
@endsection