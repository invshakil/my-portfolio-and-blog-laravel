<!-- Imported styles on this page -->
<link rel="stylesheet" href="{{URL::to('/public/assets')}}/js/datatables/responsive/css/datatables.responsive.css">
<link rel="stylesheet" href="{{URL::to('/public/assets')}}/js/select2/select2-bootstrap.css">
<link rel="stylesheet" href="{{URL::to('/public/assets')}}/js/select2/select2.css">

<!-- Bottom scripts (common) -->
<script src="{{URL::to('/public/assets')}}/js/gsap/main-gsap.js"></script>
<script src="{{URL::to('/public/assets')}}/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>
<script src="{{URL::to('/public/assets')}}/js/bootstrap.js"></script>
<script src="{{URL::to('/public/assets')}}/js/joinable.js"></script>
<script src="{{URL::to('/public/assets')}}/js/resizeable.js"></script>
<script src="{{URL::to('/public/assets')}}/js/neon-api.js"></script>
<script src="{{URL::to('/public/assets')}}/js/jquery.dataTables.min.js"></script>
<script src="{{URL::to('/public/assets')}}/js/datatables/TableTools.min.js"></script>


<!-- Imported scripts on this page -->
<script src="{{URL::to('/public/assets')}}/js/dataTables.bootstrap.js"></script>
<script src="{{URL::to('/public/assets')}}/js/datatables/jquery.dataTables.columnFilter.js"></script>
<script src="{{URL::to('/public/assets')}}/js/datatables/lodash.min.js"></script>
<script src="{{URL::to('/public/assets')}}/js/datatables/responsive/js/datatables.responsive.js"></script>
<script src="{{URL::to('/public/assets')}}/js/select2/select2.min.js"></script>
<script src="{{URL::to('/public/assets')}}/js/neon-chat.js"></script>
<script src="{{URL::to('/public/assets')}}/js/fileinput.js"></script>


{{--<script src="{{URL::to('/public/assets')}}/js/ckeditor/ckeditor.js"></script>--}}
{{--<script src="{{URL::to('/public/assets')}}/js/ckeditor/adapters/jquery.js"></script>--}}

<!-- JavaScripts initializations and stuff -->
<script src="{{URL::to('/public/assets')}}/js/neon-custom.js"></script>


<!-- Demo Settings -->
<script src="{{URL::to('/public/assets')}}/js/neon-demo.js"></script>

<link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<script>
    @if(Session::has('message'))
    var type = "{{Session::get('alert-type','info')}}";


    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif
</script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>

<!-- Initialize the editor. -->
<script>
    $(document).ready(function() {
        $('.ckeditor').summernote({
            height: 300,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: true                  // set focus to editable area after initializing summernote
        });
    });

</script>