<link rel="stylesheet" href="{{URL::to('/public/assets')}}/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
<link rel="stylesheet" href="{{URL::to('/public/assets')}}/css/font-icons/entypo/css/entypo.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
<link rel="stylesheet" href="{{URL::to('/public/assets')}}/css/bootstrap.css">
<link rel="stylesheet" href="{{URL::to('/public/assets')}}/css/neon-core.css">
<link rel="stylesheet" href="{{URL::to('/public/assets')}}/css/neon-theme.css">
<link rel="stylesheet" href="{{URL::to('/public/assets')}}/css/neon-forms.css">
<link rel="stylesheet" href="{{URL::to('/public/assets')}}/css/custom.css">
<link rel="stylesheet" href="{{URL::to('/public/assets')}}/css/skins/cafe.css">


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

<script src="{{URL::to('/public/assets')}}/js/jquery-1.11.0.min.js"></script>
<script>$.noConflict();</script>
<script src="https://code.jquery.com/jquery-1.8.0.min.js"></script>

<!--[if lt IE 9]><script src="{{URL::to('/public/assets')}}/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

<style>
    .alert {
        font-size: 16px;font-weight: bold
    }

    body .page-container .sidebar-menu
    {
        background-color: #030717!important;
    }

    body .page-container .sidebar-menu #main-menu li.active > a{
        background: #454a54!important;
    }

    .page-container .sidebar-menu #main-menu li a i {
        font-size: 25px;
    }
    body .page-container .sidebar-menu #main-menu li {
        padding: 5px;
        font-size: 16px;
    }
    body .page-container .sidebar-menu #main-menu li a {
        font-weight: bold;
        font-family: "Times New Roman";
    }

    .page-container .sidebar-menu #main-menu li ul > li > a {
        background-color: rgba(255, 255, 255, 0.15)!important;
    }
</style>