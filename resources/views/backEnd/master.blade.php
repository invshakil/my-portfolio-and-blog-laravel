<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Admin Panel"/>
    <meta name="author" content=""/>

    <meta http-equiv="expires" content="Mon, 26 Jul 1997 05:00:00 GMT"/>
    <meta http-equiv="pragma" content="no-cache"/>

    <title>@yield('title')</title>

    @include('backend.includes.include_top')


</head>
<body class="page-body page-fade-only skin-cafe">

<div class="page-container">
    <!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->

    @include('backend.'.$account_type.'.'.'navigation')

    <div class="main-content">


        <div class="row">

            <!-- Profile Info and Notifications -->
            <div class="col-md-6 col-sm-8 clearfix">

                <ul class="user-info pull-left pull-none-xsm">

                    <!-- Profile Info -->
                    <li class="profile-info dropdown">
                        <!-- add class "pull-right" if you want to place this from right -->

                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{ auth()->user()->image_path }}" alt="" class="img-circle" width="44"/>
                            {{ auth()->user()->name }}
                        </a>

                        <ul class="dropdown-menu">

                            <!-- Reverse Caret -->
                            <li class="caret"></li>

                            <!-- Profile sub-links -->
                            <li>
                                <a href="{{ route('admin.profile') }}">
                                    <i class="entypo-user"></i>
                                    Edit Profile
                                </a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>

            <!-- Raw Links -->
            <div class="col-md-6 col-sm-4 clearfix hidden-xs">

                <ul class="list-inline links-list pull-right">

                    <li>
                        <a href="{{ route('logout') }}">
                            Log Out <i class="entypo-logout right"></i>
                        </a>
                    </li>
                </ul>

            </div>

        </div>

        <hr/>


        <div class="col-md-12" style="min-height: 800px;">

            @yield('main_content')

        </div>

        <!-- Footer -->
        <footer class="main">

            &copy; 2017 <strong>Software</strong> Developed by <a href="http://www.sshakil.com"
                                                                  target="_blank">Shakil</a>

        </footer>
    </div>


</div>

{{--@include('backend.includes.modal')--}}

{{--@include('backend.includes.modal')--}}
@include('backend.includes.include_bottom')
</body>
</html>