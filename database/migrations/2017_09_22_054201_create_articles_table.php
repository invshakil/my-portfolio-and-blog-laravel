<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('article_id');
            $table->string('title');
            $table->text('description');
            $table->text('image');
            $table->text('thumbnail');
            $table->string('category');
            $table->string('tags');
            $table->integer('author');
            $table->integer('hit_count');
            $table->integer('slider_status');
            $table->integer('status');
            $table->string('meta_title');
            $table->text('meta_description');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
