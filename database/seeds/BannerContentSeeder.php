<?php

use Illuminate\Database\Seeder;

class BannerContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('banner_content')->insert([
            'info_type' => 'name',
            'description' => NULL
        ]);

        DB::table('banner_content')->insert([
            'info_type' => 'designation',
            'description' => NULL
        ]);

        DB::table('banner_content')->insert([
            'info_type' => 'intro_text',
            'description' => NULL
        ]);

        DB::table('banner_content')->insert([
            'info_type' => 'image',
            'description' => NULL
        ]);

        DB::table('banner_content')->insert([
            'info_type' => 'facebook',
            'description' => NULL
        ]);

        DB::table('banner_content')->insert([
            'info_type' => 'twitter',
            'description' => NULL
        ]);

        DB::table('banner_content')->insert([
            'info_type' => 'bitbucket',
            'description' => NULL
        ]);

        DB::table('banner_content')->insert([
            'info_type' => 'stack_overflow',
            'description' => NULL
        ]);

    }
}
