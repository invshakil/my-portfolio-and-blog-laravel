<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
            'name' => 'Shakil',
            'email' => 'admin@gmail.com',
            'password' => md5('123456'),
            'user_type' => ('1')
        ]);

    }
}
