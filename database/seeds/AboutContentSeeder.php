<?php

use Illuminate\Database\Seeder;

class AboutContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('about_content')->insert([
            'info_type' => 'name',
            'description' => NULL
        ]);

        DB::table('about_content')->insert([
            'info_type' => 'age',
            'description' => NULL
        ]);

        DB::table('about_content')->insert([
            'info_type' => 'experience',
            'description' => NULL
        ]);

        DB::table('about_content')->insert([
            'info_type' => 'image',
            'description' => NULL
        ]);

        DB::table('about_content')->insert([
            'info_type' => 'email',
            'description' => NULL
        ]);

        DB::table('about_content')->insert([
            'info_type' => 'phone',
            'description' => NULL
        ]);

        DB::table('about_content')->insert([
            'info_type' => 'website',
            'description' => NULL
        ]);

        DB::table('about_content')->insert([
            'info_type' => 'address',
            'description' => NULL
        ]);

        DB::table('about_content')->insert([
            'info_type' => 'cv',
            'description' => NULL
        ]);

        DB::table('about_content')->insert([
            'info_type' => 'blog',
            'description' => NULL
        ]);

        DB::table('about_content')->insert([
            'info_type' => 'video',
            'description' => NULL
        ]);

        DB::table('about_content')->insert([
            'info_type' => 'about',
            'description' => NULL
        ]);
    }
}
